/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.notifiable;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import android.content.DialogInterface;
import android.content.Intent;

import android.media.RingtoneManager;

import android.net.Uri;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import org.bigfoot.simplealarm.R;
import org.bigfoot.simplealarm.dialog.TextPickerDialog;
import org.bigfoot.simplealarm.dialog.TextPickerDialogFragment;

/** Fragment displaying the details of an alarm.*/
public abstract class NotifiableDetailsFragment extends Fragment
{
  private static final int SELECT_RINGTONE = 0; ///< Case for select ringtone activity return
  
  public static final String NOTIFIABLE_ID = "notifiableId";  ///< Name for notifiable Id returned value.
  
  private NotifiableData  mNotifiable = null; ///< notifiable to display
  private TextView        mName = null;  ///< Displays the alarm name
  private TextView        mRing = null;  ///< Displays the alarm ringtone
  
  /** @brief Builds the notifiable to handle.
    * @return The notifiable to handle
    */
  protected abstract NotifiableData makeNotifiable(long notifiableId);
  
  /** @brief Saves changes to database*/
  protected abstract void saveChanges();
  
  /** @return The current Notifiable
    */
  protected NotifiableData getNotifiable()
  {
    return mNotifiable;
  }

  /** @brief Called on activity view creation
    * @param[in] inflater: view inflater
    * @param[in] container: parent container
    * @param[in] savedInstanceState: saved instance state
    * @return view of the fragment.
    */
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  { 
    View v = inflater.inflate(R.layout.notifiabledetailsfragment, container, false);
    
    ViewGroup scContainer = (ViewGroup)v.findViewById(R.id.detailsContainer);
    scContainer.removeAllViews();
    scContainer.addView(setContent(inflater, scContainer));
    
    mName = (TextView) v.findViewById(R.id.detailsName);
    mRing = (TextView) v.findViewById(R.id.detailsRingtone);
    
    // set listener
    mName.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        selectName(v);
      }
    });
    
    v.findViewById(R.id.detailsRingtoneLayout).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        selectRingtone(v);
      }
    });
    
    updateGUI(getArguments().getLong(NOTIFIABLE_ID, -1));
    return v;
  }
  
  /** Sets the content of GUI */
  protected abstract View setContent(LayoutInflater inflater, ViewGroup Container);
  
  /** @brief Updates the GUI
    * @param[in] notifiableId: Id of the notifiable to use
    */
  protected void updateGUI(long notifiableId)
  {
    if(notifiableId >= 0)
    {
      mNotifiable = makeNotifiable(notifiableId);
      mName.setText(mNotifiable.getName());
      String ring = getResources().getString(R.string.defaultRing);
      Uri ringUri = mNotifiable.getRingUri();
      if(ringUri != null)
      {
        ring = RingtoneManager.getRingtone(getActivity(), ringUri).getTitle(getActivity());
      }
      mRing.setText(ring);
    }
  }
  
  /** Called when users ask for a notifiable name selection.
    * @param[in] v: view used
    */
  private void selectName(View v)
  {
    String name = (String) mName.getText();
    String dialogTitle = getResources().getString(R.string.selectAlarmName);
    
    TextPickerDialogFragment tpdf = TextPickerDialogFragment.build(
      name, dialogTitle, new DialogInterface.OnClickListener(){
        @Override
        public void onClick(DialogInterface d, int which) {
          String name = ((TextPickerDialog) d).getText();
          mNotifiable.setName(name);
          mName.setText(name);
          saveChanges();
        }
      });
    tpdf.show(getFragmentManager(), "title");
  }
  
  /** Called when users ask for a ringtone selection
    * @param[in] v: view used
    */
  private void selectRingtone(View v)
  {
    Intent i = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
    i.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, getResources().getString(R.string.selectRingtone));
    i.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, false);
    i.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, false);
    i.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE,RingtoneManager.TYPE_ALARM);
    startActivityForResult(i, SELECT_RINGTONE);
  }
  
  /** Called when a sub activity returns
    * @param[in] requestCode: request code used when launching the sub activity
    * @param[in] resultCode: result code of the sub activity
    * @param[in] intent: intent returned by the sub activity
    */
  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent intent)
  {
    switch(requestCode)
    {
      case SELECT_RINGTONE:
        switch(resultCode)
        {
          case FragmentActivity.RESULT_OK:
            Uri ringUri = intent.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
            mNotifiable.setRingUri(ringUri);
            if(ringUri != null)
            {
              String ring = RingtoneManager.getRingtone(getActivity(), ringUri).getTitle(getActivity());
              mRing.setText(ring);
              saveChanges();
            }
            break;
            
          case FragmentActivity.RESULT_CANCELED:
          default:
            break;
        }
        break;
      default:
        break;
    }
  }
}

