/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.notifiable;

import android.app.AlarmManager;
import android.app.PendingIntent;

import android.content.Context;
import android.content.Intent;

import android.net.Uri;
import android.os.Build;

/** @brief Base class used to register a Notifier.
  *
  * This class makes the relashionship between a Notifier and the device.
  * 
  */
public abstract class NotifiableRegisterer
{

  public static final String NOTIFIABLE_ID = "Id";
  
  protected Context mContext = null;  ///< context of the registerer
  
  /** @brief Base Constructor
    *
    * @param[in] context: contex of the alarm
    */
  protected NotifiableRegisterer(Context context)
  {
    this.mContext = context;
  }
  
  /** @brief Arms the notifiable. 
    *
    *  Arms the device's RTC_WAKEUP to trigger the input PendingIntent at the input time..
    *  @param[in] timeInMillis: RTC time to wakeup
    *  @param[in] i: pending intent to register
    */
  protected void alarmManagerSet(long timeInMillis, PendingIntent i)
  {
    AlarmManager alarmManager = (AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);
    if(Build.VERSION.SDK_INT  >= Build.VERSION_CODES.KITKAT)
      alarmManager.setExact(AlarmManager.RTC_WAKEUP, timeInMillis, i);
    else
      alarmManager.set(AlarmManager.RTC_WAKEUP, timeInMillis, i);
  }
  
  /** @brief Cancels a notifiable.
    *  
    *  The inupt PendingIntent will be removed from the device's AlarmManager
    *  @param[in] i: pending intent to cancel
    */
  protected void alarmManagerCancel(PendingIntent i)
  {
    AlarmManager alarmManager = (AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);
    alarmManager.cancel(i);
  }
  
  protected abstract Class getIntentClass();
  
  /** @brief Makes the pending intent for the given notifiable.
    *
    * This pending intent will be used to trigger the notifiable at the given date.
    * @param[in] notifiable: Notifiable to trigger
    * @return the newly created pending intent
    */
  protected PendingIntent makePendingIntent(NotifiableData notifiable)
  {
    Uri.Builder uriBuilder = new Uri.Builder();
    uriBuilder.appendQueryParameter(NOTIFIABLE_ID, String.format("%d", notifiable.getId()));
    Intent myIntent = new Intent(NOTIFIABLE_ID, uriBuilder.build(), mContext, getIntentClass());
    myIntent.putExtra(NOTIFIABLE_ID, notifiable.getId());
    return PendingIntent.getBroadcast(mContext, 0, myIntent,0);
  }
}
