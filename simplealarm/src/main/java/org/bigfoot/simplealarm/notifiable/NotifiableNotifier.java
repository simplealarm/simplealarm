/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.notifiable;

import java.lang.Runnable;

import android.app.Activity;

import android.content.Intent;

import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;

import android.net.Uri;

import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;

import android.view.Window;
import android.view.WindowManager;

/** @brief Notification activity
  *
  * This is the base activity started when the notification time of a
  * notifier has come.
  *
  * It handles:
  * - the ringtone
  * - the vibrator
  */
public abstract class NotifiableNotifier extends Activity {
  private NotifiableData mNotifiable = null; ///< notifiable corresponding to the intent
  private Ringtone  mRingtone  = null; ///< ringtone to play
  private Handler   mHandler   = null; ///< handler used to restart the ringtone
  private Runnable  mRingRun   = null; ///< starts playing the ringtone
  private Vibrator  mVibrator  = null; ///< vibrator
  
  
  protected long getNotifiableId(Intent i)
  {
    return i.getLongExtra(NotifiableRegisterer.NOTIFIABLE_ID, -1);
  }
  
  /** @brief Called on activity creation
    * @param[in] savedInstanceState: saved instance state
    */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    Window window = this.getWindow();
    window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                
    super.onCreate(savedInstanceState);
    
    setContentView(getContentId());
    
    setVolumeControlStream(AudioManager.STREAM_ALARM);
    
    Intent i = getIntent();
    mNotifiable = initNotifiable(i);
    
    if(mNotifiable != null)
    {
      onNotificationStart();
      mRingRun = new Runnable() {
        public void run() {
          ring();
        }
      };
      mHandler = new Handler();
      mHandler.postDelayed(new Runnable() {
        public void run() {
          close();
        }
      }, 60000);
      updateGUI();
    }
    else
    {
      close();
    }
  }
  
  /** @return build a notifiable from intent*/
  protected abstract NotifiableData buildNotifiable(Intent i);

  /** @return The notifiable to handle */
  protected abstract NotifiableData initNotifiable(Intent i);
  
  /** @return the id for the content layout*/
  protected abstract int getContentId();
  
  /** @brief Updates the GUI */
  protected abstract void updateGUI();
  
  /** @brief Called on notification start */
  protected abstract void onNotificationStart();

  /** @brief Called if a new notification is incomming while one is already running.
      @param[in] newNotifiable: the new incomming notifiable.*/
  protected abstract void handleNewNotifiable(NotifiableData newNotifiable);
  
  protected void onNewIntent(Intent i) {
    NotifiableData newNotifiable = buildNotifiable(i);
    if (newNotifiable != null){
      handleNewNotifiable(newNotifiable);
    }
  }

  /** @brief Called on activity start */
  @Override
  public void onStart() {
    super.onStart();
    mVibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
    if(mNotifiable != null)
    {
      long[] pattern = {0,700,300};
      mVibrator.vibrate(pattern, 1);
      startRinging();
    }
  }
  
  /** @brief Called on activity stop */
  @Override
  public void onStop() {
    super.onStop();
    mHandler.removeCallbacks(mRingRun);
    if(mRingtone != null)
    {
      mRingtone.stop();
    }
    if(mVibrator != null)
    {
      mVibrator.cancel();
    }
  }
  
  /** Selects the ringtone ant starts it*/
  private void startRinging() {
    Uri ringtoneUri = mNotifiable.getRingUri();
    
    // If no ringtone found...
    if(ringtoneUri == null)
    {
      ringtoneUri = RingtoneManager.getActualDefaultRingtoneUri(this, RingtoneManager.TYPE_ALARM);
    }
    
    if(mRingtone == null)
    {
      mRingtone = RingtoneManager.getRingtone(this, ringtoneUri);
      mRingtone.setStreamType(AudioManager.STREAM_ALARM);
    }
    
    ring();
  }
  
  /** Plays the ringtone */
  private void ring() {
    if(!mRingtone.isPlaying())
    {
      mRingtone.play();
    }
    mHandler.postDelayed(mRingRun, 10000 );
  }
  
  /** Closes the activity */
  protected void close() {
    NotifiableReceiver.releaseWakeLock();
    finish();
  }
}

