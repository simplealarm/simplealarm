/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.notifiable;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;

/** @brief Broadcast receiver for the notifications.
  */
public abstract class NotifiableReceiver extends BroadcastReceiver
{
  private static PowerManager.WakeLock wakeLock; ///Used to lock the device awake till the notifying
                                                 ///ativity starts
  static void acquireWakeLock(Context context){
    if(wakeLock != null)
      wakeLock.release();
    PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
    wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK |
                                        PowerManager.ACQUIRE_CAUSES_WAKEUP, "SimpleAlarm");
    wakeLock.acquire();
  }

  static void releaseWakeLock(){
    if(wakeLock != null)
      wakeLock.release();
    wakeLock = null;
  }

  protected long getNotifiableId(Intent intent)
  {
    return intent.getLongExtra(NotifiableRegisterer.NOTIFIABLE_ID, -1);
  }
  
  protected abstract Class getNotifierClass();
  
  private Intent makeNotificationIntent(Context context, Intent intent)
  {
    Intent i = new Intent(context, getNotifierClass());
    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    i.putExtra(NotifiableRegisterer.NOTIFIABLE_ID, getNotifiableId(intent));
    return i;
  }
  
  protected void startNotificationActivity(Context context, Intent intent)
  {
    acquireWakeLock(context);
    context.startActivity(makeNotificationIntent(context, intent));
  }
}
