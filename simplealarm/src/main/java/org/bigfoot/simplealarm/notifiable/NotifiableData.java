/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.notifiable;

import android.net.Uri;
/** @brief Base class for notifiable.
  *
  * This class is used to store notifiable data.
  * It is used in the software as a base class for
  * - Alarms
  * - Countdowns
  */
public abstract class NotifiableData {
  
  protected int     mState  = 0;    ///< Current state
  protected String  mName   = "Notifiable"; ///< Name displayed to the user
  private Uri     mRingUri= null; ///< Uri of the ringtone to play as notification
  private long      mId     = -1;      ///< Unique notifiable identifier 

  /** @brief Base Constructor */
  protected NotifiableData() {}
  
  /** @brief Sets the state of the notifiable
    * @param[in] state: The state of the notifiable.
    */
  public void setState(int state)
  {
    mState = state;
  }
  
  /** @brief Gets the next state of the notifiable.
    *
    * Calculates the next state of the notifiable based on the current state.
    * The current state of the alarm remains unchanged.
    * @return next state of the alarm.
    */
  public abstract int nextState();
  
  /** @brief Sets the id of the notifiable
    * @param[in] id: id of the notifiable.
    */
  public void setId(long id)
  {
    mId = id;
  }
  
  /** @brief Gets the identifier of the notifiable
    * @return Identifier of the notifiable
    */
  public long getId()
  {
    return mId;
  }
  
  /** @brief Sets the name of the notifiable
    * @param[in] name: new name
    */
  public void setName(String name)
  {
    mName = name;
  }
  
  /** @brief Sets the ringtone of the notifiable
    * @param[in] ringUri: new ringtone
    */
  public void setRingUri(Uri ringUri)
  {
    mRingUri = ringUri;
  }
  
  /** @brief Gets the name of the notifiable
    * @return Alarm's name
    */
  public String getName()
  {
    return mName;
  }
  
  /** @brief Gets the state of the notifiable.
    * @return The notifiable state.
    */
  public int getState()
  {
    return mState;
  }
  
  /** @brief Gets the notifiable's ringtone uri.
    * @return Notifiable's ringtone uri.
    */
  public Uri getRingUri()
  {
    return mRingUri;
  }
}

