package org.bigfoot.simplealarm;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;


/**
 * Let the user select between two solutions by moving the thumb of a seekbar left or right.
 */
public class ChoiceBar extends RelativeLayout {
  public static abstract class OnChoiceListener{
    public abstract void onChoice(int choice);
  }
  private int mColorFiltered  = 0;    ///< 1 if one of the image has a color filter applied
  private ImageView mLeftImage;
  private ImageView mRightImage;
  private int mLeftImageColor;
  private int mRightImageColor;
  private OnChoiceListener mOnChoiceListener;

  public ChoiceBar(Context context) {
    super(context);
    init(null, 0);
  }

  public ChoiceBar(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(attrs, 0);
  }

  public ChoiceBar(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    init(attrs, defStyle);
  }

  private void init(AttributeSet attrs, int defStyle) {
    inflate(getContext(), R.layout.choicebarlayout, this);
    mLeftImage = (ImageView) findViewById(R.id.leftImage);
    mRightImage = (ImageView) findViewById(R.id.rightImage);
    // Load attributes
    final TypedArray a = getContext().obtainStyledAttributes(
        attrs, R.styleable.ChoiceBar, defStyle, 0);

    mLeftImage.setImageDrawable(a.getDrawable(R.styleable.ChoiceBar_leftImage));
    mRightImage.setImageDrawable(a.getDrawable(R.styleable.ChoiceBar_rightImage));
    mLeftImageColor = a.getColor(R.styleable.ChoiceBar_leftSelectColor, Color.BLACK);
    mRightImageColor = a.getColor(R.styleable.ChoiceBar_rightSelectColor, Color.BLACK);

    a.recycle();

    SeekBar choiceBar = (SeekBar) findViewById(R.id.choiceBar);
    choiceBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
      @Override
      public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (progress > 90) {
          if (mColorFiltered == 0) {
            mRightImage.setColorFilter(mRightImageColor);
            mColorFiltered = 1;
          }
        } else if (progress < 10) {
          if (mColorFiltered == 0) {
            mLeftImage.setColorFilter(mLeftImageColor);
            mColorFiltered = 1;
          }
        } else {
          if (mColorFiltered == 1) {
            mLeftImage.setColorFilter(Color.argb(0, 0, 0, 0));
            mRightImage.setColorFilter(Color.argb(0, 0, 0, 0));
            mColorFiltered = 0;
          }
        }
      }

      @Override
      public void onStartTrackingTouch(SeekBar seekBar) {

      }

      @Override
      public void onStopTrackingTouch(SeekBar seekBar) {
        int progress = seekBar.getProgress();
        if (progress > 90) {
          if(mOnChoiceListener != null)
            mOnChoiceListener.onChoice(1);
        } else if (progress < 10) {
          if(mOnChoiceListener != null)
            mOnChoiceListener.onChoice(0);
        } else {
          seekBar.setProgress(50);
        }
      }
    });
  }

  public void setOnChoiceListener(OnChoiceListener OnChoiceListener) {
    this.mOnChoiceListener = OnChoiceListener;
  }
}
