/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.widget;

import org.bigfoot.simplealarm.pluginmanager.PluginManager;
import org.bigfoot.simplealarmwidgets.PluginReceiver;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Pair;

import org.bigfoot.simplealarm.MainSQL;

public class WidgetsSQL
{
  private static final String WIDGET_TABLE = "Widgets";     ///< name of the Widgets table
  private static final String ID_COLUMN    = "_id";         ///< name of the private key field
  private static final String WID_COLUMN   = "WidgetId";    ///< name of the widget Id field
  private static final String WTYPE_COLUMN = "WidgetType";  ///< name of the widget type field
  
  private static final String UPDATER_TABLE  = "WidgetUpdaters";
  private static final String UID_COLUMN     = "_id";
  private static final String UPACKAGE_COLUMN= "PackageName";
  private static final String UCLASS_COLUMN  = "ClassName";
  private static final String UNAME_COLUMN   = "UpdaterName";
  
  private MainSQL mMainSQL = null;  ///< main database
  
  public static class UpdaterDescriptor
  {
    private String mPackageName = "";
    private String mType = "";
    private String mName = "";
    UpdaterDescriptor(String packageName, String uType, String name)
    {
      this.mPackageName = packageName;
      this.mType = uType;
      this.mName = name;
    }
    public String getName() { return mName;}
    public String getType() { return mType;}
    public String getPackageName() { return mPackageName;}
  }
  
  /** @brief Base Constructor
    * @param[in] context: current context
    */
  public WidgetsSQL(Context context)
  {
    mMainSQL = new MainSQL(context);
  }
  
  /** @brief Called on the database creation
    * @param[in] db: Database to modify
    */
  public static void onCreate(SQLiteDatabase db, Context context)
  {
    final String create = "create table " + WIDGET_TABLE + "(" + 
      ID_COLUMN     + " integer primary key autoincrement, "   +
      WID_COLUMN    + " integer, " + 
      WTYPE_COLUMN  + " text"      + ");";
    db.execSQL(create);
    createUpdaterTable(db, context);
  }


  private static void addPlugin(SQLiteDatabase db,
                                String packageName,
                                PluginReceiver.UpdaterDescriptor[] descriptors)
  {
      for(PluginReceiver.UpdaterDescriptor descriptor:descriptors)
      {
          ContentValues values = new ContentValues();
          values.put(UPACKAGE_COLUMN,   packageName);
          values.put(UCLASS_COLUMN,     descriptor.getUpdaterClassName());
          values.put(UNAME_COLUMN,      descriptor.getUpdaterName());
          db.insert(UPDATER_TABLE, null, values);
      }
  }
  
  private static void createUpdaterTable(SQLiteDatabase db, Context context)
  {
    final String create = "create table " + UPDATER_TABLE + "(" + 
      UID_COLUMN      + " integer primary key autoincrement, "  +
      UPACKAGE_COLUMN + " text, " + 
      UCLASS_COLUMN   + " text, " +
      UNAME_COLUMN    + " text"   + ");";
    db.execSQL(create);
    // Adds the widget updaters.
    for(Pair<String, PluginReceiver> pr : new PluginManager(context).getAllPlugins())
    {
        addPlugin(db, pr.first, pr.second.getWidgetUpdaters());
    }
  }

  /** @brief Open the database
    * @return The opened SQLite database.
    */
  private SQLiteDatabase open() throws SQLException
  {
    return mMainSQL.getWritableDatabase();
  }
  
  /** @brief Called on database upgrade
    * 
    * This function performs update on the alarm table from a version to the other.
    * @param[in] db: Database to upgrade
    * @param[in] oldVersion: Old version of the database
    * @param[in] newVersion: version to which the database must be upgrade
    * @param[in] context
    */
  public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion, Context context)
  {
    switch(oldVersion)
    {
      case 1:
      case 2:
      case 3:
        onCreate(db, context);
        break;
      case 4:
        createUpdaterTable(db, context);
        break;
      default:
        break;
    }
  }
  
  /** Checks whether an updater is still in use.
    * @param[in] wtype: type of the widget updater to look for.
    * @return true if the updater is still in use.
    */
  public boolean updaterUnused(String wtype)
  {
    String[] column = {WTYPE_COLUMN};
    String[] args = {wtype};
    SQLiteDatabase db = open();
    
    Cursor widgetCursor = db.query(WIDGET_TABLE, column,
        String.format("%s = ?", WTYPE_COLUMN), args ,
        null, null,
        ID_COLUMN, null);
    boolean unUsed = (widgetCursor.getCount() == 0);
    widgetCursor.close();
    db.close();
    return unUsed;
  }
  
  /** Sets the widget updater type for the given widget id.
    * @param[in] id: identifier of the widget as provided to the WidgetsProvider
    * @param[in] wtype: type of the widget updater to use for this widget.
    */
  public void setWidgetTypeForId(int id, String wtype)
  {
    SQLiteDatabase db = open();
    ContentValues values = new ContentValues();
    values.put(WID_COLUMN,   id);
    values.put(WTYPE_COLUMN, wtype); 
    // creates the entry if the widget is not registered in the database
    if(db.update(WIDGET_TABLE, values, String.format("%s = %d", WID_COLUMN, id), null) == 0)
    {
      db.insert(WIDGET_TABLE, null, values);
    }
    db.close();
  }
  
  /** Gets the type of the widget updater to use for the given widget id.
    * @param[in] id: identifier of the widget as provided to the WidgetsProvider
    * @return Type name of the widget updater
    */
  public String getWidgetTypeFromId(int id)
  {
    final String[] column = {WTYPE_COLUMN};
    String type = "";
    SQLiteDatabase db = open();
    Cursor widgetCursor   = db.query(WIDGET_TABLE, column,
      String.format("%s = %d", WID_COLUMN, id) , null,
      null, null,
      ID_COLUMN, null);

    if(widgetCursor.moveToFirst())
    {
      final int wTypeIndex  = widgetCursor.getColumnIndex(WTYPE_COLUMN);
      type =  widgetCursor.getString(wTypeIndex);
    }
    widgetCursor.close();
    db.close();
    return type;
  }
  
  /** Removes the parameters of the input widget identifier
    * @param[in] id: identifier of the widget to remove
    */
  public int removeWidgetParameters(int id)
  {
    SQLiteDatabase db = open();
    int ans = db.delete(WIDGET_TABLE, String.format("%s = %d", WID_COLUMN, id), null);
    db.close();
    return ans;
  }
  
  public UpdaterDescriptor getDescriptorForType(String className)
  {
    final String[] columns = {UPACKAGE_COLUMN, UCLASS_COLUMN, UNAME_COLUMN};
    final String[] args    = {className};
    SQLiteDatabase db = open();
    Cursor updaterCursor = db.query(UPDATER_TABLE, columns,
      String.format("%s = ?", UCLASS_COLUMN), args, null, null, ID_COLUMN);
    final int uPackageIndex  = updaterCursor.getColumnIndex(UPACKAGE_COLUMN);
    final int uClassIndex    = updaterCursor.getColumnIndex(UCLASS_COLUMN);
    final int uNameIndex     = updaterCursor.getColumnIndex(UNAME_COLUMN);
    updaterCursor.moveToNext();
    UpdaterDescriptor element = new UpdaterDescriptor(
        updaterCursor.getString(uPackageIndex),
        updaterCursor.getString(uClassIndex),
        updaterCursor.getString(uNameIndex));
    updaterCursor.close();
    db.close();
    return element;
  }
  
  /** Gets the updaters list */
  public ArrayList<UpdaterDescriptor> getUpdaters()
  {
    final String[] columns = {UPACKAGE_COLUMN, UCLASS_COLUMN, UNAME_COLUMN};
    SQLiteDatabase db = open();
    Cursor updaterCursor = db.query(UPDATER_TABLE, columns, null, null, null, null, ID_COLUMN);
    final int uPackageIndex  = updaterCursor.getColumnIndex(UPACKAGE_COLUMN);
    final int uClassIndex    = updaterCursor.getColumnIndex(UCLASS_COLUMN);
    final int uNameIndex     = updaterCursor.getColumnIndex(UNAME_COLUMN);
    ArrayList<UpdaterDescriptor> ans = new ArrayList<>();
    while (updaterCursor.moveToNext())
    {
      UpdaterDescriptor element = new UpdaterDescriptor(
        updaterCursor.getString(uPackageIndex),
        updaterCursor.getString(uClassIndex),
        updaterCursor.getString(uNameIndex));
      ans.add(element);
    }
    updaterCursor.close();
    db.close();
    return ans;
  }
  
  public void removePlugin(String packageName)
  {
    final String[] args = {packageName};
    SQLiteDatabase db = open();
    db.delete(UPDATER_TABLE, String.format("%s = ?", UPACKAGE_COLUMN), args);
    db.close();
  }

  public void addPlugin(String packageName, PluginReceiver.UpdaterDescriptor[] descriptors)
  {
    removePlugin(packageName);
    
    SQLiteDatabase db = open();
    addPlugin(db, packageName, descriptors);
    db.close();
  }
}
