/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.widget;

import org.bigfoot.simplealarm.SimpleAlarm;
import org.bigfoot.simplealarmwidgets.BaseWidgetUpdater;

import java.util.Calendar;
import java.util.Iterator;
import java.util.HashMap;
import java.lang.Math;

import android.app.AlarmManager;
import android.app.PendingIntent;

import android.appwidget.AppWidgetProvider;
import android.appwidget.AppWidgetManager;

import android.content.Context;
import android.content.Intent;

import android.net.Uri;

import android.widget.RemoteViews;

import org.bigfoot.simplealarm.ClockWidget;

/** Widget Provider used for all widgets provided by SimpleAlarm.
 */
public class WidgetsProvider extends AppWidgetProvider
{
  public static final String WIDGET_EXTRA = "Widget";
  ///Map storing the widget updaters associated to their names
  private static final HashMap<String, BaseWidgetUpdater> mUpdaterMap = new HashMap<>();
  ///Next update
  private static long mNextUpdate = -1;
  
  /** Register a widget updater to the updater map
   *  @param[in] widgetUpdater: widget updater to register
   */
  private static void registerWidgetUpdater(BaseWidgetUpdater widgetUpdater)
  {
    mUpdaterMap.put(widgetUpdater.getClass().getName(), widgetUpdater);
  }
  
  /** Unregister an unused widget updater to the updater map
   *  @param[in] updaterType: widget updater to remove
   */
  private static void removeWidgetUpdater(String updaterType, WidgetsSQL widgetSQL)
  {
    if(widgetSQL.updaterUnused(updaterType))
    {
      mUpdaterMap.remove(updaterType);
    }
  }
  
  /** Gets the widget updater corresponding to input the name from the updater map
   *  @param[in] className: key used to get the corresponding updater
   *  @param[in] context: context used to instanciate the updater
   *  @param[in] widgetSQL: database managing widgets
   *  @return The widget updater for the given name.
   */
  private static BaseWidgetUpdater getWidgetUpdater(String className, Context context, WidgetsSQL widgetsSQL)
  {
    BaseWidgetUpdater updater = mUpdaterMap.get(className);
    UpdaterLoader updaterLoader = new UpdaterLoader();
    try
    {
      if(updater == null)
      {
        WidgetsSQL.UpdaterDescriptor descriptor = widgetsSQL.getDescriptorForType(className);
        String packageName = descriptor.getPackageName();
        updater = updaterLoader.loadUpdater(packageName, className, context);
        
        if(updater == null)
          updater = new ClockWidget();
        else
          registerWidgetUpdater(updater);
      }
    } catch(Exception e)
    {
      updater = new ClockWidget();
    }
    return updater;
  }

  @Override
  public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds)
  {
    updateViews(context, appWidgetManager, appWidgetIds);
  }
  
  @Override
  public void onDeleted(Context context, int[] appWidgetIds)
  {
    WidgetsSQL widgetsSQL = new WidgetsSQL(context);
    // removes all widget descriptors from the database
    String updaterType;
    for(int appWidgetId : appWidgetIds)
    {
      updaterType = widgetsSQL.getWidgetTypeFromId(appWidgetId);
      widgetsSQL.removeWidgetParameters(appWidgetId);
      removeWidgetUpdater(updaterType, widgetsSQL);
    }
  }
  
  public static String getUpdaterTypeForId(int appWidgetId, Context context)
  {
    return new WidgetsSQL(context).getWidgetTypeFromId(appWidgetId);
  }
  
  public static void setUpdaterTypeForId(int appWidgetId, String uType, Context context)
  {
    WidgetsSQL widgetsSQL = new WidgetsSQL(context);
    widgetsSQL.setWidgetTypeForId(appWidgetId, uType);
    int[] appWidgetIds = {appWidgetId};
    updateViews(context, AppWidgetManager.getInstance(context), appWidgetIds);
  }
  
  /** Gets the updater to use for the given widget Id.
    * @param[in] appWidgetId: Id of the widget.
    * @param[in] context: Context used to build the widget if needed
   *  @param[in] widgetsSQL: Widgets Database
    * @return Updater for the widget.
    */
  private static BaseWidgetUpdater getUpdaterForId(int appWidgetId, Context context, WidgetsSQL widgetsSQL)
  {
    String updaterType = getUpdaterTypeForId(appWidgetId, context);
    if (updaterType.equals("")) {
      Intent i = new Intent(context, WidgetsSetupActivity.class);
      i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      i.putExtra(WidgetsProvider.WIDGET_EXTRA, appWidgetId);
      context.startActivity(i);
    }
    return getWidgetUpdater(updaterType, context, widgetsSQL);
  }
  
  /** Makes the pending intent that will launch SimpleAlarm on click.
   *  @param[in] context: context to create the intent
   *  @param[in] appWidgetId: Id of the widget to put in the intent extra
   *  @return The pending intent that will launch SimpleAlarm.
   */
  private static PendingIntent mkSimpleAlarmPendingIntent(Context context, int appWidgetId)
  {
    Uri.Builder uriBuilder = new Uri.Builder();
    uriBuilder.appendQueryParameter("Widget_Id", String.format("%d", appWidgetId));
    // Create an Intent to launch SimpleAlarm
    Intent intent = new Intent("Widget_Id", uriBuilder.build(),context, SimpleAlarm.class);
    intent.putExtra(WidgetsProvider.WIDGET_EXTRA, appWidgetId);
    return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
  }
  
  /** Updates the given views
    * @param[in] context: Context used to build the updaters
    * @param[in] appWidgetManager: Widget Manager handling the widget update
    * @param[in] appWidgetIds: Ids of the widgets to upadte
    */
  public static void updateViews(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds)
  {
    long updateTime = 0;

    WidgetsSQL widgetsSQL = new WidgetsSQL(context);

    final long now = Calendar.getInstance().getTimeInMillis();

    for (int appWidgetId : appWidgetIds)
    {
      BaseWidgetUpdater widgetUpdater = getUpdaterForId(appWidgetId, context, widgetsSQL);
      
      // Builds the Widget
      RemoteViews views = widgetUpdater.getViews(context, appWidgetId, now);
      if(views != null)
      {
        views.setOnClickPendingIntent(
          widgetUpdater.getClickableView(),
          mkSimpleAlarmPendingIntent(context, appWidgetId));
        // Updates the Widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
        
        if( updateTime == 0 )
        {
          updateTime = widgetUpdater.getNextUpdateMillis(context, appWidgetId);
        }
        else
        {
          updateTime = Math.min(updateTime, widgetUpdater.getNextUpdateMillis(context, appWidgetId));
        }
      }
    }
    // Registers the next update
    registerNextUpdate(context, updateTime, now);
    // Removes unused updaters
    //Set<String> keySet = mUpdaterMap.keySet();
    Iterator<String> iterator = mUpdaterMap.keySet().iterator();
    while(iterator.hasNext())
    {
      String key = iterator.next();
      if(widgetsSQL.updaterUnused(key))
        iterator.remove();
    }
  }
  
  /** Register an RTC alarm for the next widget update.
    * @param[in] context: used to get the AlarmManager
    * @param[in] updateTime: next time to update widgets
    * @param[in] now: actual time
    */
  private static void registerNextUpdate(Context context, long updateTime, long now)
  {
    if(mNextUpdate < now)
    {
      mNextUpdate = updateTime;
    }
    else
    {
      mNextUpdate = Math.min(updateTime, mNextUpdate);
    }
    
    if(mNextUpdate > now)
    {
      Intent i = new Intent(context, WidgetsService.class);
      PendingIntent p = PendingIntent.getService(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
      AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
      alarmManager.set(AlarmManager.RTC, mNextUpdate, p);
    }
  }
}

