/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.widget;

import org.bigfoot.simplealarmwidgets.BaseWidgetUpdater;

import dalvik.system.PathClassLoader;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import android.content.Context;
import android.util.Log;
import java.lang.ClassLoader;

class UpdaterLoader {
  public UpdaterLoader() {}
  
  private ClassLoader getUpdaterClassLoader(String packageName, Context context) throws Exception
  {
    PackageManager pkgManager = context.getPackageManager();
    ApplicationInfo appInfo = pkgManager.getApplicationInfo(packageName, 0);
    return new PathClassLoader(appInfo.sourceDir, context.getClassLoader());
  }
  
  public BaseWidgetUpdater loadUpdater(String packageName, String className, Context context)
  {
    BaseWidgetUpdater updater = null;
    try
    {
      Class<?> cl = Class.forName(className, true, getUpdaterClassLoader(packageName, context));
      updater = (BaseWidgetUpdater) cl.newInstance();
      updater.setPackageContext(context.createPackageContext(packageName, 0));
    }
    catch(Exception ex)
    {
      Log.w("SimpleAlarm", "- Exception", ex);
    }
    return updater;
  }
}

