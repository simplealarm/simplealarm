/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.widget;

import android.os.Bundle;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;

import org.bigfoot.simplealarm.R;

public class WidgetsSetupActivity extends Activity
  implements WidgetsSelectFragment.OnUpdaterSelectedListener
{
  /** Called when the activity is first created. */
  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.widgetssetupactivity);
    int appWidgetId = getIntent().getIntExtra(WidgetsProvider.WIDGET_EXTRA, -1);
    
    Fragment widgetFragment = WidgetsSelectFragment.build(appWidgetId);
    FragmentTransaction transaction = getFragmentManager().beginTransaction();
    transaction.replace(R.id.mainContainer, widgetFragment);
    transaction.commit();
  }
  
  @Override
  public void onUpdaterSelected(String updaterType)
  {
    finish();
  }
}
