/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.widget;

import java.util.List;

import android.app.Fragment;

import android.os.Bundle;

import android.app.Activity;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import org.bigfoot.simplealarm.R;

public class WidgetsSelectFragment extends Fragment
{
  private OnUpdaterSelectedListener mListener = null;
  private String mCurrentType = "";
  private static final String WIDGET_ID = "widgetId";  ///< Name for notifiable Id returned value.
  
  private class UpdaterAdapter extends ArrayAdapter<WidgetsSQL.UpdaterDescriptor>
  {
    private Context mContext = null;
    private List<WidgetsSQL.UpdaterDescriptor> mUpdaters = null;
    public UpdaterAdapter(Context context, List<WidgetsSQL.UpdaterDescriptor> updaters)
    {
      super(context, R.layout.widgetelement, updaters);
      this.mContext = context;
      this.mUpdaters = updaters;
    }
    
    @Override
    public View getView( int index, View convertView, ViewGroup parent )
    {
      View v = convertView;
      if(v == null)
      {
        LayoutInflater inflater = (LayoutInflater) this.mContext
          .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.widgetelement, parent, false);
      }
      RadioButton rb = (RadioButton)v.findViewById(R.id.selected);
      TextView tv = (TextView)v.findViewById(R.id.widgetName);
      
      rb.setChecked(mUpdaters.get(index).getType().equals(mCurrentType));
      tv.setText(mUpdaters.get(index).getName());
      return v;
    }
  }
  
  public interface OnUpdaterSelectedListener{
    void onUpdaterSelected(String updaterType);
  }
  
  @Override
  public void onAttach(Activity activity)
  {
    super.onAttach(activity);
    try {
      mListener = (OnUpdaterSelectedListener) activity;
    }
    catch(ClassCastException e) {
      throw new ClassCastException(activity.toString() + " must implement OnUpdaterSelectedListener");
    }
  }
  
  /** @brief Builder
    * @param[in] appWidgetId: Identifier of the widget
    * @return Returns the newly created WidgetsSelectFragment.
    */
  public static WidgetsSelectFragment build(int appWidgetId)
  {
    WidgetsSelectFragment f = new WidgetsSelectFragment();
    Bundle args = new Bundle();
    args.putInt(WIDGET_ID, appWidgetId);
    f.setArguments(args);
    return f;
  }
  
  private List<WidgetsSQL.UpdaterDescriptor> mUpdaters = null;
  
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  {
    final int appWidgetId = getArguments().getInt(WIDGET_ID, -1);
    buildUpdaterList(appWidgetId);

    View view = inflater.inflate(R.layout.widgetselect, container, false);
    ListView lvWidgets = (ListView) view.findViewById(R.id.lvWidgets);
    lvWidgets.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id)
      {
        String uType = ((WidgetsSQL.UpdaterDescriptor) parent.getAdapter().getItem(position)).getType();
        WidgetsProvider.setUpdaterTypeForId(appWidgetId, uType, getActivity());
        mListener.onUpdaterSelected(uType);
      }
    });
    lvWidgets.setAdapter(new UpdaterAdapter(getActivity(), mUpdaters));
    return view;
  }
  
  private void buildUpdaterList(int appWidgetId)
  {
    mCurrentType = WidgetsProvider.getUpdaterTypeForId(appWidgetId, getActivity());
    mUpdaters = new WidgetsSQL(getActivity()).getUpdaters();
    // Sets the updater type to a default existing one.
    if(mCurrentType.isEmpty())
    {
        mCurrentType = mUpdaters.get(0).getType();
        WidgetsProvider.setUpdaterTypeForId(appWidgetId, mCurrentType, getActivity());
    }
  }
}

