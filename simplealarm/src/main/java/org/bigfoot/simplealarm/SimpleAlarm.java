/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm;

import android.os.Bundle;

import android.support.v4.view.ViewPager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import org.bigfoot.simplealarm.alarm.AlarmsFragment;
import org.bigfoot.simplealarm.countdown.CountdownFragment;

/** Main Activity class.
  */
public class SimpleAlarm extends FragmentActivity
{
  private static final int ALARM = 0;
  private static final int COUNTDOWN = 1;

  private NotifierAdapter mAdapter;
  private ViewPager mPager;

  
  /** Called when the activity is first created. */
  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
    if (mPager == null) {
        if (mAdapter == null)
        {
            mAdapter = new NotifierAdapter(getSupportFragmentManager());
            mPager = (ViewPager) findViewById(R.id.pager);
            mPager.setAdapter(mAdapter);
        }
    }
  }
  
  public class NotifierAdapter extends FragmentPagerAdapter {
    public NotifierAdapter(FragmentManager fm) {
      super(fm);
    }
    
    @Override
    public int getCount(){
      return 2;
    }
    
    @Override
    public CharSequence getPageTitle(int position){
      String ans;
      switch(position)
      {
        case COUNTDOWN:
          ans = getResources().getString(R.string.countdowns_title);
          break;
        case ALARM:
        default:
          ans = getResources().getString(R.string.alarms_title);
          break;
      }
      return ans;
    }
    
    @Override
    public Fragment getItem(int position){
      Fragment f;
      switch(position)
      {
        case COUNTDOWN:
          f = CountdownFragment.build();
          break;
        case ALARM:
        default:
          f = AlarmsFragment.build();
          break;
      }
      return f;
    }
  }
  
}

