/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.common;

import android.view.View.OnClickListener;

/** @brief Helper for array adapter clicklistener
  */
public abstract class IndexOnClickListener implements OnClickListener {
  protected int mIndex = 0; ///< index of the related item
  
  /** @brief Base Constructor
    * @param[in] index: index to link to*/
  public IndexOnClickListener(int index)
  {
    super();
    this.mIndex = index;
  }
}
