/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.bigfoot.simplealarm.alarm.AlarmSQL;
import org.bigfoot.simplealarm.countdown.CountdownSQL;

/** @brief Receiver for the boot finish intent.
  * 
  * This class is in charge to handle boot actions.
  */
public class BootReceiver extends BroadcastReceiver
{
  @Override
  /** @brief Registers the active alarms on start up
    * @param[in] context: current context
    * @param[in] intent: received intent
    */
  public void onReceive(Context context, Intent intent)
  {
    AlarmSQL alarmDB = new AlarmSQL(context);
    alarmDB.onBootRegister();
    CountdownSQL countdownDB = new CountdownSQL(context);
    countdownDB.onBootRegister();
  }
}

