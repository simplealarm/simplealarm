/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.common;

import android.support.v4.app.Fragment;

import android.content.Context;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import org.bigfoot.simplealarm.R;

public abstract class SelectableAdapterFragment<T> extends Fragment
{
  private SelectableAdapterAMC mActionModeCallback = null;
  protected SelectableAdapter<T> mAdapter = null;

    /** @brief Get the elements to add to the element list.
    *
    * @return the list of elements
    */
  protected abstract T[] onPopulate();
  
  /** @brief called when the adapter mode changes.
    *
    * Called before applying changes to the adapter.
    * @param[in] mode: new adapter mode.
    */
  protected abstract void onModeChange(int mode);
  
  /** @brief Called when the given element in the adapter is removed
    *
    * @param[in] element: element to delete.
    */
  protected abstract void onDeleteElement(T element);
 
  /** @brief Called to create the view content of the fragment
    * @param[in] inflater: view inflater
    * @param[in] container: parent container
    * @param[in] savedInstanceState: saved instance state
    * @return view of the fragment.
    */
  protected abstract View buildFragment(
    LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);
  
  /** @brief Gets the adapter to use.
    * @param[in] context: context of the fragment
    * @return Adapter to use.
    */
  protected abstract SelectableAdapter buildFragmentAdapter(Context context);
  
  /** @brief Called on activity view creation
    * @param[in] inflater: view inflater
    * @param[in] container: parent container
    * @param[in] savedInstanceState: saved instance state
    * @return view of the fragment.
    */
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  {
    mActionModeCallback = new SelectableAdapterAMC(this);
    View view = buildFragment(inflater, container, savedInstanceState);
    mAdapter = buildFragmentAdapter(getActivity());
    return view;
  }
  
  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater)
  {
    if(menu.findItem(R.id.action_add_element) == null)
    {
      menuInflater.inflate(R.menu.main_actions, menu);
    }
    super.onCreateOptionsMenu(menu, menuInflater);
  }
  
  /** Called when the add button is pressed*/
  protected abstract void onActionElementAdd();
  
  /** Called when the delete button is pressed*/
  private void onActionElementDelete() {getActivity().startActionMode(mActionModeCallback);}
  
  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    boolean ans;
    // Handle presses on the action bar items
    switch (item.getItemId())
    {
      // add new alarm pressed
      case R.id.action_add_element:
        onActionElementAdd();
        ans = true;
        break;
      // delete alarm pressed
      case R.id.action_del:
        onActionElementDelete();
        ans = true;
        break;
      default:
        ans = super.onOptionsItemSelected(item);
    }
    return ans;
  }
  
  /** @brief Updates the alarm adapter with alarms from the database */
  private void gatherAlarms()
  {
    mAdapter.clear();
    mAdapter.addAll(onPopulate());
    mAdapter.notifyDataSetChanged();
  }
  
  /** @brief Deletes alarm selected in the alarm adapter.
    */
  public void delSelection()
  {
    int i = 0;
    int elementIndex;
    T element;
    for(Integer index : mAdapter.getSelection())
    {
      elementIndex = index + i;
      element = mAdapter.getItem(elementIndex);
      // index is corrected from the number of
      // elements already deleted
      onDeleteElement(element);
      // removes the alarm from gui
      mAdapter.remove(element);
      --i;
    }
    mAdapter.notifyDataSetChanged();
  }
  
  /** @brief Sets the alarm adapter mode.
    *
    * @param[in] mode: new mode of the alarm adapter
    */
  public void setMode(int mode)
  {
    mAdapter.setMode(mode);
    onModeChange(mode);
    mAdapter.notifyDataSetChanged();
  }
  
  @Override
  public void onStart()
  {
    super.onStart();
    // displays alarm in the database
    gatherAlarms(); 
  }
}
