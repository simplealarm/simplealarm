/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.common;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import org.bigfoot.simplealarm.R;

/** Alarms Contextual menu */
public class SelectableAdapterAMC implements ActionMode.Callback
{
  private static ActionMode mActionMode = null;
  private static void setActionMode(ActionMode am)
  {
    finishActionMode();
    mActionMode = am;
  }

  public static void finishActionMode()
  {
    if(mActionMode != null) {
      mActionMode.finish();
      mActionMode = null;
    }
  }

  private SelectableAdapterFragment mListFragment = null;///< Changes will be applied on this alarm fragment.
  
  /** @brief Base Constructor
    *
    * @param[in] listFragment: Fragment to interact with.
    */
  public SelectableAdapterAMC(SelectableAdapterFragment listFragment)
  {
    mListFragment = listFragment;
  }

  @Override
  public boolean onCreateActionMode(ActionMode mode, Menu menu) {
    setActionMode(mode);
    MenuInflater inflater = mode.getMenuInflater();
    inflater.inflate(R.menu.alarms_mode, menu);
    return true;
  }
  
  @Override
  public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
    boolean ans;
    switch (item.getItemId())
    {
      // removes the selected alarms
      case R.id.action_del:
        mListFragment.delSelection();
        ans = true;
        finishActionMode();
        break;
      default:
        ans = false;
        break;
    }
    return ans;
  }
  
  @Override
  public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
    // changes the AlarmAdapter to display the deletion mode
    mListFragment.setMode(SelectableAdapter.SELECT_MODE);
    return true;
  }
  
  @Override
  public void onDestroyActionMode(ActionMode mode) {
    // changes the AlarmAdapter back to display the active alarms
    mListFragment.setMode(SelectableAdapter.DISPLAY_MODE);
  }
}
