/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.common;

import java.util.List;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import org.bigfoot.simplealarm.R;

/** @brief Adapter for element with a selection checkbox
  */
public class CheckAdapter extends ArrayAdapter<CheckElement> {
  
  private final List<CheckElement> mChecked; ///< list of elements
  private final Context            mContext; ///< context
  
  /** @brief Base Constructor
    * @param[in] context: current context
    * @param[in] checked: list of checked elements
    */
  public CheckAdapter(Context context, List<CheckElement> checked) {
    super(context, R.layout.checkrowlayout, checked);
    this.mChecked = checked;
    this.mContext = context;
  }
  
  @Override
  /** @brief Returns a view corresponding to the given index
    * @param[in] index: index of the element for which to create the view
    * @param[in] convertView: unused view already created
    * @param[in] parent: container for the returned view
    * @return View containing values of the element.
    */
  public View getView( int index, View convertView, ViewGroup parent ) {
    View rowView = convertView;
    
    if(rowView == null)
    {
      LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      rowView = inflater.inflate(R.layout.checkrowlayout, parent, false);
    }
    
    CheckElement checkElement = mChecked.get(index);
    if(checkElement != null)
    {
      CheckBox select       = (CheckBox) rowView.findViewById(R.id.checkrowSelect);
      TextView description  = (TextView) rowView.findViewById(R.id.checkrowDescription);
      
      // set click listener
      select.setOnClickListener(new IndexOnClickListener(index) {
        @Override
        public void onClick(View v) {
          CheckAdapter.this.select(mIndex, ((CheckBox) v).isChecked());
        }
      });
      
      // set values
      select.setChecked(checkElement.getSelected());
      description.setText(checkElement.getDescription());
    }
    return rowView;
  }
  
  /** @brief Toggle the check box.
    * @param[in] index: index of the element to check.
    * @param[in] selected: true to check the checkbox
    */
  private void select(int index, boolean selected) {
    mChecked.get(index).setSelected(selected);
  }
  
  /** @brief Base listener called on click*/
  abstract class IndexOnClickListener implements OnClickListener {
    int mIndex = 0; ///< index of the element in the CheckAdapter.
    
    /** @brief Base Constructor
      * @param[in] index: index of the element linked to this callback*/
    public IndexOnClickListener(int index)
    {
      super();
      this.mIndex = index;
    }
  }
}
