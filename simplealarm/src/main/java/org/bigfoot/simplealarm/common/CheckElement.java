/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.common;

/** @brief Base Element for CheckAdater
  *
  * Adds a selection parameter to the template class
  */
public abstract class CheckElement<T> {
  private boolean mSelected = false;  ///< Used to determine wether the element is selected
  protected T mValue = null;  ///< value associated to the element
  
  /** @brief Base Constructor
    * @param[in] selected: true to make this element selected
    * @param[in] value: value associated with the element
    */
  public CheckElement(boolean selected, T value) {
    setSelected(selected);
    setValue(value);
  }
  
  /** @brief Sets the selection flag.
    * @param[in] selected: true to make this element selected
    */
  public void setSelected(boolean selected) {this.mSelected = selected;}
  
  /** @brief Gets the selection state.
    * @return True if the element is activated
    */
  public boolean getSelected() {return this.mSelected;}
  
  /** @brief Sets the value of the element.
    * @param[in] value: new value for the element.
    */
  private void setValue(T value) {this.mValue = value;}
  
  /** @brief Gets the actual value of the element.
    * @return The value of the element.
    */
  public T  getValue() {return mValue;}
  
  /** @brief Returns a printable value.
    *
    * Returns a string that will be displayed to the user
    * in the adapter. This text is expected to described
    * the value contained in the element.
    * @return A description of the element's value.
    */
  public abstract String getDescription();
}
