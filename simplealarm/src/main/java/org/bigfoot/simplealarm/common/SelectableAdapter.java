/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.common;

import java.util.List;
import java.util.TreeSet;

import android.content.Context;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;

import org.bigfoot.simplealarm.R;

/** @brief Array adapter providing selectable elements
  *
  * The SelectableAdapter provides two GUI modes:
  * - Select mode with a checkbox for selection
  * - Display mode where the checkbox is hidden or replaced with an other element
  */
public abstract class SelectableAdapter<T> extends ArrayAdapter<T> {
  protected static final int SELECT_MODE     = 0; ///< Displays the selection checkbox instead of the alternative view.
  protected static final int DISPLAY_MODE    = 1; ///< Displays the complete view

  private final Context    mContext;   ///< current context
  protected final List<T>    mElements;  ///< list of elements
  
  private TreeSet<Integer>   mSelection; ///< selected elements
  private int                mMode = DISPLAY_MODE; ///< mode of the adapter
  private final int                mLayoutResource; ///< layout to inflate for the view
  
  /** @brief Base Constructor
    * @param[in] context: current context
    * @param[in] layoutResource: rows layout
    * @param[in] elements: elements to display
    */
  protected SelectableAdapter(Context context, int layoutResource, List<T> elements)
  {
    super(context, layoutResource, elements);
    this.mElements = elements;
    this.mContext =  context;
    this.mLayoutResource = layoutResource;
    this.mSelection = new TreeSet<>();
  }
  
  /** @brief Sets the mode of the adapter
    *
    * Handles two modes:
    * - DISPLAY_MODE: default mode
    * - SELECT_MODE: mode for item selection
    * @param[in] mode: new mode for the adapter
    */
  public void setMode(int mode)
  {
    switch(mode)
    {
      case SELECT_MODE:
        mSelection = new TreeSet<>();
        mMode = SELECT_MODE;
        break;
      case DISPLAY_MODE:
      default:
        mMode = DISPLAY_MODE;
        break;
    }
  }
  
  /** returns the current mode of the adapter
    * @return current mode.
    */
  protected int getMode()
  {
    return mMode;
  }
  
  /** @brief Returns the list of selected elements.
    * @return Set of selected elements
    */
  public TreeSet<Integer> getSelection()
  {
    return mSelection;
  }
  
  /** @brief Returns the alternative view for display mode.
    *
    * @param[in] index: index of the element in the adapter
    * @param[in] convertView: optional old view to recycle
    * @param[in] parent: parent container
    * @return view with the element data
    */
  protected View getDisplayView(int index, View convertView, ViewGroup parent)
  {
    return null;
  }
  
  /** @brief Calculates the view to display the element.
    *
    * @param[in] index: index of the element in the adapter
    * @param[in] convertView: optional old view to recycle
    * @param[in] parent: parent container
    * @return view with the element data
    */
  @Override
  public View getView( int index, View convertView, ViewGroup parent )
  {
    LinearLayout rowView = (LinearLayout) convertView;
    CheckBox selection;
    View display;
    
    // Creates the rowView if not recycled
    if(rowView == null)
    {
      //Makes a new rowview
      rowView = new LinearLayout(mContext);
      rowView.setLayoutParams(new ListView.LayoutParams(
        LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        
      //Makes the selection box
      selection = new CheckBox(mContext);
      selection.setId(R.id.selectionViewId);
      LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
      lp.gravity = Gravity.CENTER;
      selection.setLayoutParams(lp);
      rowView.addView(selection);
      
      //Makes the displayView
      display = getDisplayView(index, null, rowView);
      if(display != null)
      {
        display.setId(R.id.displayViewId);
        display.setLayoutParams(lp);
        rowView.addView(display);
      }
      
      //Makes the custom row content
      LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      View rowContent = inflater.inflate(mLayoutResource, parent, false);
      rowView.addView(rowContent);
    }
    else
    //... or gather the existing view otherwise
    {
      selection = (CheckBox) rowView.findViewById(R.id.selectionViewId);
      View oldDisplay   = rowView.findViewById(R.id.displayViewId);
      display   = getDisplayView(index, oldDisplay, rowView);
      if(display != null)
      {
        if(display != oldDisplay)
        {
          display.setId(R.id.displayViewId);
          if(oldDisplay != null)
          {
            rowView.removeViewAt(1);
          }
          rowView.addView(display, 1);
        }
      }
    }
    
    selection.setOnClickListener(new SelectListener(index));
    
    if(display != null)
    {
      switch(getMode())
      {
        case SELECT_MODE:
          selection.setVisibility(View.VISIBLE);
          display.setVisibility(View.GONE);
          break;
        case DISPLAY_MODE:
          selection.setVisibility(View.GONE);
          display.setVisibility(View.VISIBLE);
        default:
          break;
      }
    }
    else
      selection.setVisibility(View.VISIBLE);
      
    selection.setChecked(mSelection.contains(index));
    
    buildView(index, rowView, mMode);
    return rowView;
  }
  
  /** @brief Selection listener
    * Adds or removes the clicked element to/from the selection list.
    *
    * @param[in] index: index of the element.
    */
  private class SelectListener extends IndexOnClickListener
  {
    SelectListener(int index)
    {
      super(index);
    }
    
    @Override
    public void onClick(View v)
    {
      if(((CheckBox) v).isChecked())
      {
        mSelection.add(mIndex);
      }
      else
      {
        mSelection.remove(mIndex);
      }
    }
  }
  
  /** @brief Builds the new row view
    *
    * @param[in] index: index of the element in the adapter
    * @param[in] rowView: view displaying the element.
    * @param[in] mode: current mode of the adapter
    */
  protected abstract void buildView(int index, View rowView, int mode);
}

