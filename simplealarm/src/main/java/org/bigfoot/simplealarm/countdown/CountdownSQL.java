/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.countdown;

import android.content.ContentValues;
import android.content.Context;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import android.net.Uri;

import org.bigfoot.simplealarm.MainSQL;

public class CountdownSQL {
  private static final String COUNTDOWN_TABLE = "Countdown";  ///< name of the table
  private static final String ID_COLUMN       = "_id";      ///< name of the private key field
  private static final String NAME_COLUMN     = "name";     ///< name of the alarm name field
  private static final String SECONDS_COLUMN  = "secondes"; ///< name of the repeat field
  private static final String ACTIVE_COLUMN   = "active";   ///< name of the active field
  private static final String RINGURI_COLUMN  = "ringUri";  ///< name of the ring uri field
  private static final String START_TS_COLUMN = "startTimeStamp";///< start time of the countdown
  
  private MainSQL mMainSQL = null;  ///< main database
  private CountdownRegisterer mRegisterer = null;  ///< registerer
  
  /** @brief Base Constructor
    * @param[in] context: current context
    */
  public CountdownSQL(Context context)
  {
    mMainSQL = new MainSQL(context);
    mRegisterer = new CountdownRegisterer(context);
  }
  
  /** @brief Called on the database creation
    * @param[in] db: Database to modify
    */
  public static void onCreate(SQLiteDatabase db)
  {
    final String create = "create table " + COUNTDOWN_TABLE + "(" + 
      ID_COLUMN      + " integer primary key autoincrement, " +
      NAME_COLUMN    + " text not null, " +
      SECONDS_COLUMN + " integer, " +
      ACTIVE_COLUMN  + " integer, " +
      START_TS_COLUMN+ " integer, " +
      RINGURI_COLUMN + " text"      + ");";
    db.execSQL(create);
  }
  
  /** @brief Called on database upgrade
    * 
    * This function performs update on the alarm table from a version to the other.
    * @param[in] db: Database to upgrade
    * @param[in] oldVersion: Old version of the database
    * @param[in] newVersion: version to which the database must be upgrade
    */
  public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
  {
    switch(oldVersion)
    {
      case 1:
        onCreate(db);
        break;
      case 2:
        db.execSQL("alter table " + COUNTDOWN_TABLE + " add column " + START_TS_COLUMN + " integer;");
        break;
      default:
        break;
    }
    
  }
  
  /** Returns the alarm corresponding to the given Id.
    * @param[in] id: id of the alarm to find.
    * @return The alarm corresponding to the id.
    */
  public CountdownData getCountdownFromId(long id)
  {
    // Query to selection the alarm in the database
    final String selQuery = String.format("%s = %d", ID_COLUMN, id);
    SQLiteDatabase db = open();
    Cursor countdownCursor = db.query(COUNTDOWN_TABLE, null,
      selQuery , null,
      null, null,
      ID_COLUMN, null);
      
    // Colums index
    final int nameIndex     = countdownCursor.getColumnIndex(NAME_COLUMN);
    final int activeIndex   = countdownCursor.getColumnIndex(ACTIVE_COLUMN);
    final int secondsIndex  = countdownCursor.getColumnIndex(SECONDS_COLUMN);
    final int ringUriIndex  = countdownCursor.getColumnIndex(RINGURI_COLUMN);
    final int startTSIndex  = countdownCursor.getColumnIndex(START_TS_COLUMN);
    
    // Move to the first element (might not be required)
    countdownCursor.moveToFirst();
    
    // ringUri
    Uri ringUri = null;
    String stringUri = countdownCursor.getString(ringUriIndex);
    
    if(stringUri.length() != 0)
    {
      ringUri = Uri.parse(stringUri);
    }
    
    // Make the AlarmData from the database
    CountdownData countdown = new CountdownData(countdownCursor.getString(nameIndex),
      countdownCursor.getInt(activeIndex), countdownCursor.getInt(secondsIndex),
      countdownCursor.getLong(startTSIndex));
    countdown.setId(id);
    countdown.setRingUri(ringUri);

    countdownCursor.close();
    db.close();
    return countdown;
  }

  /** Returns the list of countdowns in the database
   *  @return Array containing the countdowns
   */ 
  public CountdownData[] getCountdowns()
  {
    String[] columns = new String[1];
    columns[0] = ID_COLUMN;
    SQLiteDatabase db = open();
    Cursor countdownCursor = db.query(COUNTDOWN_TABLE, columns,
      null, null,
      null, null,
      ID_COLUMN, null);
    final int nCountdowns  = countdownCursor.getCount();
    final int idsIndex = countdownCursor.getColumnIndex(ID_COLUMN);
    CountdownData[] countdownArray = new CountdownData[nCountdowns];
    
    countdownCursor.moveToFirst();
    for(int i = 0; i < nCountdowns; ++i)
    {
      countdownArray[i] = getCountdownFromId(countdownCursor.getLong(idsIndex));
      countdownCursor.moveToNext();
    }

    countdownCursor.close();
    db.close();
    return countdownArray;
  }

  /** @brief Register all active alarms on device boot */
  public void onBootRegister()
  {
    SQLiteDatabase db = open();
    String[] columns = new String[1];
    columns[0] = ID_COLUMN;
    Cursor countdownsCursor = db.query(COUNTDOWN_TABLE, columns,
        String.format("%s != %s", ACTIVE_COLUMN, CountdownData.INACTIVE), null,
        null, null,
        ID_COLUMN, null);

    final int idsIndex = countdownsCursor.getColumnIndex(ID_COLUMN);

    countdownsCursor.moveToFirst();
    for(int i = 0; i < countdownsCursor.getCount(); ++i)
    {
      CountdownData countdown = getCountdownFromId(countdownsCursor.getLong(idsIndex));
      mRegisterer.setCountdown(countdown);
      countdownsCursor.moveToNext();
    }
    countdownsCursor.close();
    db.close();
  }

  /** @brief Updates the database with the countdown
    * 
    * Adds or replace the alarm in the database
    * @param[in] countdown: countdown to add or update
    */
  private void updateDataBase(CountdownData countdown)
  {
    // Update the database
    Uri ringUri = countdown.getRingUri();
    String ringString = "";
    if(ringUri != null)
    {
      ringString = ringUri.toString();
    }
    ContentValues values = new ContentValues();
    values.put(NAME_COLUMN    , countdown.getName());
    values.put(ACTIVE_COLUMN  , countdown.getState());
    values.put(SECONDS_COLUMN , countdown.getDuration());
    values.put(START_TS_COLUMN, countdown.getStartTime());
    values.put(RINGURI_COLUMN , ringString);
    
    long id = countdown.getId();
    
    SQLiteDatabase db = open();
    // Adds the alarm to the data base...
    if(id == -1)
    {
      countdown.setId(db.insert(COUNTDOWN_TABLE, null, values));
    }
    // ... or update the existing entry
    else
    {
      db.update(COUNTDOWN_TABLE, values, String.format("%s = %d", ID_COLUMN, id), null);
    }
    db.close();
    mRegisterer.setCountdown(countdown);
  }
  
  /** @brief Open the database
    * @return The opened SQLite database.
    */
  private SQLiteDatabase open() throws SQLException
  {
    return mMainSQL.getWritableDatabase();
  }
  
  /** @brief Update the countdown after notification.
    * 
    * Called after an countdown notification.
    * @param[in] countdown: notifield countdown
    */
  public void countdownNotified(CountdownData countdown)
  {
    // Deactivate the countdown
    countdown.setState(CountdownData.INACTIVE);
    updateDataBase(countdown);
    mRegisterer.setCountdown(countdown);
  }
  
  /** @brief Adds an countdown to the database
    * @param[in] countdown: countdown to add
    * @return the time in milliseconds at which the alarm will ring
    */
  public void setCountdown(CountdownData countdown)
  {
    updateDataBase(countdown);
    mRegisterer.setCountdown(countdown);
  }
  
  /** @brief Removes the countdown.
    * 
    * Firstly unregister the countdown, then deletes it from the database.
    * @param[in] countdown: countdown to remove
    */
  public void delCountdown(CountdownData countdown)
  {
    SQLiteDatabase db = open();
    db.delete(COUNTDOWN_TABLE, String.format("%s = %d", ID_COLUMN, countdown.getId()), null);
    db.close();
    countdown.setId(-1);
    countdown.setState(CountdownData.INACTIVE);
    mRegisterer.setCountdown(countdown);
  }
}

