/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.countdown;

import java.util.List;

import android.content.Context;

import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.TextView;

import org.bigfoot.simplealarm.R;
import org.bigfoot.simplealarm.SimpleAlarm;
import org.bigfoot.simplealarm.common.SelectableAdapter;
import org.bigfoot.simplealarm.common.IndexOnClickListener;
import org.bigfoot.simplealarm.views.CountdownStateButton;

/** @brief Array adapter dedicated to alarm
  */
public class CountdownAdapter extends SelectableAdapter<CountdownData> {
  private final SimpleAlarm  mContext;   ///< current context
  
  private StateClickListener mOnStateClick= null;       ///< Action to perform on state click
  private TextClickListener  mOnDurationClick = null;   ///< Action to perform on duration click
  private TextClickListener  mOnDescriptionClick = null;///< Action to perform on duration click
  
  /** @brief Callback interface for the user clicks on alarm state button
    */
  interface StateClickListener {
    /** @brief Called on statebutton click.
      * @param[in] v: clicked view
      * @param[in] index: index of the element in the adapter
      */
    void onStateClick(CountdownStateButton v, int index);
  }
  
  /** @brief Callback interface for the user clicks callback
    */
  interface TextClickListener {
    /** @brief Called on time view click.
      * @param[in] v: clicked view
      * @param[in] index: index of the element in the adapter
      */
    void onClick(TextView v, int index);
  }
  
  /** @brief Sets the listener for click on the duration element.
    * @param[in] onDurationClick: new listener
    */
  public void setOnDurationClickListener(TextClickListener onDurationClick) {
    mOnDurationClick = onDurationClick;
  }
  
  /** @brief Sets the listener for click on the description element.
    * @param[in] onDescriptionClick: new listener
    */
  public void setOnDescriptionClickListener(TextClickListener onDescriptionClick) {
    mOnDescriptionClick = onDescriptionClick;
  }
  
  /** @brief Sets the state button click listener.
    * @param[in] onStateClick: new listener
    */
  public void setOnStateClickListener(StateClickListener onStateClick)
  {
    mOnStateClick = onStateClick;
  }
  
  /** @brief Base Constructor
    * @param[in] context: current context
    * @param[in] countdowns: countdowns list
    */
  public CountdownAdapter(Context context, List<CountdownData> countdowns) {
    super(context, R.layout.countdownrowlayout, countdowns);
    this.mContext = (SimpleAlarm) context;
  }
  
  /** @brief Returns the alternative view for display mode.
    *
    * @param[in] index: index of the element in the adapter
    * @param[in] convertView: optional old view to recycle
    * @param[in] parent: parent container
    * @return view with the countdown data
    */
  protected View getDisplayView(int index, View convertView, ViewGroup parent)
  {
    CountdownStateButton displayView = (CountdownStateButton) convertView;
    if(convertView == null)
      displayView = new CountdownStateButton(mContext);
    
    CountdownData countdown = mElements.get(index);
    displayView.setState(countdown.getState());
    displayView.setOnClickListener(new IndexOnClickListener(index) {
      @Override
      public void onClick(View v) {
        onStateClick(v, mIndex);
      }
    });
    
    return displayView;
  }

  @Override
  public void buildView(int index, View rowView, int mode)
  {
    CountdownData countdown = mElements.get(index);
    
    TextView description  = (TextView) rowView.findViewById(R.id.description);
    Button   duration     = (Button) rowView.findViewById(R.id.duration);
    
    String text;
    switch( countdown.getState())
    {
      case CountdownData.ACTIVE:
      case CountdownData.ELAPSED:
        text = countdown.getRemainingText();
        break;
      default:
        text = countdown.getDurationText();
    }
    description.setText(countdown.getName());
    duration.setText(text);
    
    switch(mode)
    {
      case SELECT_MODE:
        duration.setClickable(false);
        description.setClickable(false);
        break;
      case DISPLAY_MODE:
      default:
        duration.setOnClickListener(new IndexOnClickListener(index) {
          @Override
          public void onClick(View v) {
            onDurationClick(v, mIndex);
          }
        });
        description.setOnClickListener(new IndexOnClickListener(index) {
          @Override
          public void onClick(View v) {
            onDescriptionClick(v, mIndex);
          }
        });
        break;
    }
  }
    
  /** @brief Called when user clicks the state button
    *
    * @param[in] v: clicked view
    * @param[in] index: index of the clicked element
    */
  private void onStateClick(View v, int index)
  {
    if(mOnStateClick != null)
      mOnStateClick.onStateClick((CountdownStateButton)v, index);
  }
  
  /** @brief Called when user clicks on the time widget
    *
    * @param[in] v: clicked view
    * @param[in] index: index of the clicked element
    */
  private void onDurationClick(View v, int index)
  {
    if(mOnDurationClick != null && getMode() == DISPLAY_MODE)
      mOnDurationClick.onClick((TextView)v, index);
  }
  
  /** @brief Called when user clicks on the description widget
    *
    * @param[in] v: clicked view
    * @param[in] index: index of the clicked element
    */
  private void onDescriptionClick(View v, int index)
  {
    if(mOnDescriptionClick != null && getMode() == DISPLAY_MODE)
      mOnDescriptionClick.onClick((TextView)v, index);
  }
}

