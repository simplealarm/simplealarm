/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.countdown;

import android.app.PendingIntent;
import android.content.Context;
import android.util.Log;
import org.bigfoot.simplealarm.notifiable.NotifiableRegisterer;

public class CountdownRegisterer extends NotifiableRegisterer{
  //public static final String COUNTDOWN_ID = "Id"; ///< Name for the Countdown Id parameter
  
  public CountdownRegisterer(Context context)
  {
    super(context);
  }
  
  /** @brief Arms the given countdown.
    *
    * Checks the state of the countdown and arms it if required.
    * @param[in] countdown: countdown to arm
    */
  public void setCountdown(CountdownData countdown)
  {
    long id = countdown.getId();
    // If the Id is null, the countdown is not in the database, nothing is done
    if(id != -1)
    {
      // creates the pending intent for alarm notification
      PendingIntent pendingIntent = makePendingIntent(countdown);
      
      int state = countdown.getState();
      
      switch(state)
      {
        case CountdownData.INACTIVE:
          alarmManagerCancel(pendingIntent);
          break;
        case CountdownData.ACTIVE:
          // Alarm time
          long duration = (long) countdown.getDuration() * 1000;
          long ringTime = countdown.getStartTime() + duration;
          alarmManagerSet(ringTime, pendingIntent);
          break;
        case CountdownData.ELAPSED:
          break;
        default:
          Log.w("SimpleAlarm", "- Unhandled State");
          break;
      }
    }
  }
  
  @Override
  protected Class getIntentClass(){return CountdownReceiver.class;}
}

