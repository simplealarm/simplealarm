/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.countdown;

import java.lang.Runnable;
import java.util.ArrayList;

import android.app.Activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.os.Bundle;
import android.os.Handler;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ListView;
import android.widget.TextView;

import org.bigfoot.simplealarm.R;
import org.bigfoot.simplealarm.common.SelectableAdapter;
import org.bigfoot.simplealarm.common.SelectableAdapterFragment;
import org.bigfoot.simplealarm.dialog.DurationPickerDialog;
import org.bigfoot.simplealarm.dialog.DurationPickerDialogFragment;
import org.bigfoot.simplealarm.dialog.TextPickerDialog;
import org.bigfoot.simplealarm.notifiable.NotifiableDetailsFragment;
import org.bigfoot.simplealarm.views.CountdownStateButton;


/** @brief Fragment to display the alarms.
  */
public class CountdownFragment extends SelectableAdapterFragment<CountdownData>
{
  private static final int COUNTDOWN_DETAILS_RESULT = 0;  ///< Alarm details result.
  
  private ListView mLVCountdown      = null;
  private CountdownSQL mCountdownSQL = null;  ///< Database storing the countdowns.
  private Runnable mUpdater = null;    ///< updates active counters
  private Handler mHandler = null;    ///< Used to post delayed actions
  
  /** @brief Builder
    * @return Returns the newly created CountdownFragment.
    */
  public static CountdownFragment build()
  {
    return new CountdownFragment();
  }
  
  /** Updates the countdown */
  private void updater() {
    mAdapter.notifyDataSetChanged();    
    mHandler.postDelayed(mUpdater, 1000 );
  }
  
  @Override
  protected CountdownData[] onPopulate()
  {
    return mCountdownSQL.getCountdowns();
  }
  
  @Override
  protected void onModeChange(int mode)
  {}
  
  @Override
  protected View buildFragment(
    LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  {
    mCountdownSQL = new CountdownSQL(getActivity());
    
    View view = inflater.inflate(R.layout.countdownfragment, container, false);
    
    mUpdater = new Runnable() {
      public void run() {
        updater();
      }
    };
    mHandler = new Handler();
    mHandler.postDelayed(mUpdater, 1000);
        
    mLVCountdown = (ListView) view.findViewById(R.id.lvCountdown);
    mLVCountdown.setItemsCanFocus(true);
    return view;
  }
  
  @Override
  protected SelectableAdapter buildFragmentAdapter(Context context)
  {
    CountdownAdapter adapter = new CountdownAdapter(getActivity(), new ArrayList<CountdownData>());
    adapter.setOnStateClickListener(new CountdownAdapter.StateClickListener() {
      @Override
      public void onStateClick(CountdownStateButton v, int index)
      {
        stateChanged(v, index);
      }
    });
    adapter.setOnDurationClickListener(new CountdownAdapter.TextClickListener() {
      @Override
      public void onClick(TextView v, int index)
      {
        showDurationSelection(index);
      }
    });
    adapter.setOnDescriptionClickListener(new CountdownAdapter.TextClickListener() {
      @Override
      public void onClick(TextView v, int index)
      {
        showDetails(index);
      }
    });
    
    mLVCountdown.setAdapter(adapter);
    return adapter;
  }
  
  /** @brief Called on fragment creation
    * @param[in] savedInstanceState: saved instance state
    */
  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
  }
  
  /** @brief Deletes countdown.
    * Cancels the given countdown and removes it from database.
    * @param[in] countdown: element to remove
    */
  @Override
  protected void onDeleteElement(CountdownData countdown)
  {
    // deletes the countdown from database
    // and removes notification intent
    mCountdownSQL.delCountdown(countdown);
  }
  
  /** @brief Callback for user click on the alarm state button in the alarm listview
    *
    * @param[in] v: clicked view
    * @param[in] index: index of the clicked alarm
    */
  private void stateChanged(CountdownStateButton v, int index)
  {
    CountdownData countdown = mAdapter.getItem(index);
    countdown.setState(countdown.nextState());
    update(countdown);
    v.setState(countdown.getState());
  }
  
  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    switch(requestCode)
    {
      // AlarmDetails activity returned
      case COUNTDOWN_DETAILS_RESULT:
        switch(resultCode)
        {
          case Activity.RESULT_OK:
            mAdapter.notifyDataSetChanged();
            break;
          case Activity.RESULT_CANCELED:
          default:
            break;
        }
      default:
        break;
    }
  }
  
  @Override
  protected void onActionElementAdd()
  {
    showDurationSelection(-1);
  }
  
  /** @brief Sets the countdown the given alarm.
   *  Creates or Modifies an alarm in the alarm database and arms it if required.
   *  @param[in] duration: countdown duration
   *  @param[in] countdown: countdown to set, if null, a new countdown will be created
   */
  private void setCountdown(int duration, CountdownData countdown)
  {
    // if alarm is null creates a new one
    if(countdown == null)
    {
      countdown = new CountdownData();
      countdown.setName(getResources().getString(R.string.newCountdown));
      // add the new alarm in the gui
      mAdapter.add(countdown);
    }
    
    // update the alarm with the hour and minute
    countdown.setDuration(duration);
    
    // updates both database and Gui
    update(countdown);
  }
  
  private void update(CountdownData countdown)
  {
    if(countdown != null)
    {
      mCountdownSQL.setCountdown(countdown);
      mAdapter.notifyDataSetChanged();
    }
  }
  
  private void showDetails(int index)
  {
    long countdownId = mAdapter.getItem(index).getId();
    
    Intent i = new Intent(getActivity(), CountdownDetails.class);
    i.putExtra(NotifiableDetailsFragment.NOTIFIABLE_ID, countdownId);
    startActivityForResult(i, COUNTDOWN_DETAILS_RESULT);
  }
  
  private void showDurationSelection(int index)
  {
    int duration = 120;
    String pickerTitle = getResources().getString(R.string.newCountdown);
    
    CountdownData countdown = null;
    if(index >= 0)
    {
      countdown = mAdapter.getItem(index);
      pickerTitle = countdown.getName();
      duration = countdown.getDuration();
    }
    
    OnDurationSetListener onValidate = new OnDurationSetListener(countdown);
    
    DurationPickerDialogFragment tpdf = DurationPickerDialogFragment.build(
      duration, pickerTitle, onValidate);
    tpdf.show(getFragmentManager(), "timePickerDialog");
  }

  private class OnDescriptionSetListener
    implements DialogInterface.OnClickListener
  {
    private CountdownData mCountdown = null;  ///< alarm to update

    @Override
    public void onClick(DialogInterface d, int which)
    {
      TextPickerDialog dialog = (TextPickerDialog) d;
      mCountdown.setName(dialog.getText());
      update(mCountdown);
    }
  }
  
  /** @brief Click Listener for the DurationPickerDialogFragment
    *
    * Should be bound to the positive button,
    * it will tells the OnTimeSetListener to apply the changes
    * when it will be triggered.
    */
  private class OnDurationSetListener
    implements DialogInterface.OnClickListener
  {
    private CountdownData mCountdown = null;  ///< alarm to update
    
    /** Base constructor
      * @param[in] countdown: countdown to update once triggered
      */
    OnDurationSetListener(CountdownData countdown)
    {
      super();
      this.mCountdown = countdown;
    }
    
    /** @brief Applies the selected time to the AlarmData attribute.
      * @param[in] d: dialog interface
      * @param[in] which: dialog button
      */
    @Override
    public void onClick(DialogInterface d, int which)
    {
      DurationPickerDialog dialog = (DurationPickerDialog) d;
      int duration = dialog.getDuration();
      setCountdown(duration, mCountdown);
    }
  }
}

