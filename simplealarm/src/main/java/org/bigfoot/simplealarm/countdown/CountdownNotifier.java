/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.countdown;

import android.content.Intent;

import android.widget.TextView;
import android.widget.Toast;

import org.bigfoot.simplealarm.ChoiceBar;
import org.bigfoot.simplealarm.R;
import org.bigfoot.simplealarm.notifiable.NotifiableData;
import org.bigfoot.simplealarm.notifiable.NotifiableNotifier;

/** Activity launched if the countdown must ring */
public class CountdownNotifier extends NotifiableNotifier {

  private CountdownSQL  mSQL       = null; ///< database containing countdowns
  private CountdownData mCountdown = null; ///< countdown corresponding to the intent
  

  @Override
  protected NotifiableData buildNotifiable(Intent i)
  {
    final long countdownId = getNotifiableId(i);
    CountdownData ans = null;
    if(countdownId >= 0)
    {
      ans = mSQL.getCountdownFromId(countdownId);
    }
    return ans;
  }

  protected NotifiableData initNotifiable(Intent i)
  {
    mSQL = new CountdownSQL(this);
    mCountdown = (CountdownData) buildNotifiable(i);
    return mCountdown;
  }
  
  protected void onNotificationStart()
  {
    mCountdown.setState(CountdownData.ELAPSED);
    mSQL.setCountdown(mCountdown);
  }
  
  /** @brief If a new countdown elapses while still ringing, set the countdown as elapsed.*/
  @Override
  protected void handleNewNotifiable(NotifiableData newNotifiable)
  {
    newNotifiable.setState(CountdownData.ELAPSED);
    String message = String.format(getResources().getString(R.string.countdownElapsedMessage),
                                   newNotifiable.getName());
    Toast.makeText( getApplicationContext(), message, Toast.LENGTH_LONG ).show();
  }
  
  /** Returns the id for the content layout*/
  protected int getContentId()
  {
    return R.layout.countdownnotifier;
  }
  
  /** @brief Updates the GUI */
  protected void updateGUI() {
    TextView nameTV = (TextView) findViewById(R.id.notifierTitle);
    ChoiceBar choiceBar = (ChoiceBar) findViewById(R.id.countdownChoicebar);
    
    nameTV.setText(mCountdown.getName());
    choiceBar.setOnChoiceListener(new ChoiceBar.OnChoiceListener() {
      @Override
      public void onChoice(int choice) {
        if (choice == 0)
          CountdownNotifier.this.close();
        else
          CountdownNotifier.this.stop();
      }
    });
  }
  
  /** Stops the alarm */
  private void stop() {
    mSQL.countdownNotified(mCountdown);
    close();
  }
}

