/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.countdown;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import org.bigfoot.simplealarm.R;
import org.bigfoot.simplealarm.dialog.DurationPickerDialog;
import org.bigfoot.simplealarm.dialog.DurationPickerDialogFragment;
import org.bigfoot.simplealarm.notifiable.NotifiableData;
import org.bigfoot.simplealarm.notifiable.NotifiableDetailsFragment;
import org.bigfoot.simplealarm.views.CountdownStateButton;

/** Fragment displaying the details of an alarm.*/
public class CountdownDetailsFragment extends NotifiableDetailsFragment
{
  private CountdownSQL  mCountdownSQL = null; ///< Database storing the countdowns.
  private CountdownData mCountdown = null;    ///< Alarm parameters
  
  private TextView      mDuration = null;     ///< Duration text
  private CountdownStateButton  mState = null;///< Countdown state button
  private TextView      mStateTxt = null;     ///< Hint for state
  
  /** @brief Builder
    * @param[in] notifiableId: Identifier of the notifiable
    * @return Returns the newly created CountdownDetailsFragment.
    */
  public static CountdownDetailsFragment build(long notifiableId)
  {
    CountdownDetailsFragment f = new CountdownDetailsFragment();
    Bundle args = new Bundle();
    args.putLong(NOTIFIABLE_ID, notifiableId);
    f.setArguments(args);
    return f;
  }
  
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  { 
    mCountdownSQL = new CountdownSQL(getActivity());
    return super.onCreateView(inflater, container, savedInstanceState);
  }
  
  @Override
  protected NotifiableData makeNotifiable(long notifiableId)
  {
    return mCountdownSQL.getCountdownFromId(notifiableId);
  }
  
  @Override
  protected void saveChanges()
  {
    mCountdownSQL.setCountdown(mCountdown);
  }
  
  @Override
  protected View setContent(LayoutInflater inflater, ViewGroup container)
  {
    View v = inflater.inflate(R.layout.countdowndetailsfragment, container, false);
    
    mDuration  = (TextView) v.findViewById(R.id.countdownDetailsTime);
    mState     = (CountdownStateButton) v.findViewById(R.id.countdownDetailsState);
    mStateTxt  = (TextView) v.findViewById(R.id.countdownDetailsStateHint);
    
    // set listener
    mDuration.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        selectDuration(v);
      }
    });
    
    return v;
  }
  
  /** @brief Updates the GUI
    * @param[in] alarmId: Id of the alarm to use
    */
  @Override
  public void updateGUI(long alarmId)
  {
    super.updateGUI(alarmId);
    if(alarmId >= 0)
    {
      mCountdown = (CountdownData) getNotifiable();
      setState(mCountdown.getState());
      setDurationGUI();
    }
  }
  
  /** @brief Prompt the user for a ring time
    * @param[in] v: view used to select the ringtime
    */
  private void selectDuration(View v)
  {
    String pickerTitle = mCountdown.getName();
    int duration  = mCountdown.getDuration();
    
    DurationPickerDialogFragment tpdf = DurationPickerDialogFragment.build(
      duration, pickerTitle,
      new DialogInterface.OnClickListener(){
        @Override
        public void onClick(DialogInterface d, int which) {
          DurationPickerDialog dialog = (DurationPickerDialog) d;
          mCountdown.setDuration(dialog.getDuration());
          saveChanges();
          setDurationGUI();
        }
      });
    tpdf.show(getFragmentManager(), "durationPickerDialog");
  }
  
  private void setState(int state)
  {
    mState.setState(state);
    
    int textId;
    switch(state)
    {
      case CountdownData.ACTIVE:
        textId = R.string.countdown_active;
        break;
      case CountdownData.ELAPSED:
        textId = R.string.countdown_elapsed;
        break;
      case CountdownData.INACTIVE:
      default:
        textId = R.string.countdown_inactive;
        break;
    }
    mStateTxt.setText(getResources().getString(textId));
    saveChanges();
  }
  
  /** @brief Displays time as text*/
  private void setDurationGUI()
  {
    mDuration.setText(mCountdown.getDurationText());
  }
}

