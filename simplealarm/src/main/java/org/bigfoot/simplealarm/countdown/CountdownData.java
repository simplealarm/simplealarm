/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.countdown;

import java.util.Calendar;

import org.bigfoot.simplealarm.notifiable.NotifiableData;

public class CountdownData extends NotifiableData
{
  public final static int INACTIVE = 0; ///< Inactive state
  public final static int ACTIVE   = 1; ///< Active state
  public final static int ELAPSED  = 2; ///< ringing
  
  private int     mDuration  = 0;      ///< seconds duration
  private long    mStartTime = 0;      ///< time at which the countdown was started

  /** @brief Constructor
    * @param[in] name: name for the new alarm
    * @param[in] state: state of the new alarm
    * @param[in] seconds: countdown duration
    * @param[in] start: time stamp of when the countdown was started
    */
  public CountdownData(String name, int state, int seconds, long start)
  {
    this.mName   = name;
    this.mState  = state;
    this.mDuration = seconds;
    this.mStartTime = start;
  }
  
  public CountdownData() {}
  
  private int getRemaining()
  {
    int ans = 0;
    if(mState != CountdownData.INACTIVE)
    {
      int elapsed = (int) ((Calendar.getInstance().getTimeInMillis() - mStartTime)/1000);
      ans = mDuration - elapsed;
    }
    return ans;
  }
  
  public String getRemainingText()
  {
    return durationToText(getRemaining());
  }
  /** @brief Gets the ring time as text
    * @return Ringtime as text.
    */
  public String getDurationText()
  {
    return durationToText(mDuration);
  }
  
  private static String durationToText(int inSeconds)
  {
    String format = "%02d:%02d:%02d";
    if(inSeconds < 0)
    {
      inSeconds = -inSeconds;
      format = "-%02d:%02d:%02d";
    }
    
    int hours   = inSeconds / 3600;
    int rest    = inSeconds % 3600;
    int mins    = rest / 60;
    int seconds = rest % 60;
    
    // computes and display the toast for next alarm time
    return String.format(format, hours, mins, seconds);
  }
  
  /** @brief Sets the state of the countdown
    * @param[in] state: The state of the countdown.
    * 
    * Sets the countdown in one of the states:
    * - ACTIVE
    * - INACTIVE
    * - RINGING
    */
  public void setState(int state)
  {
    switch(state)
    {
      case(CountdownData.ACTIVE):
        // Sets the start time only if the coundown is inactive
        if(mState == CountdownData.INACTIVE)
          mStartTime = Calendar.getInstance().getTimeInMillis();
      case(CountdownData.INACTIVE):
      case(CountdownData.ELAPSED):
        break;
      default:
        state = CountdownData.INACTIVE;
    }
    mState = state;
  }
  
  public int nextState()
  {
    int ans = CountdownData.INACTIVE;
    switch(mState)
    {
      case(CountdownData.INACTIVE):
        ans = CountdownData.ACTIVE;
      case(CountdownData.ACTIVE):
      case(CountdownData.ELAPSED):
        break;
      default:
        ans = CountdownData.INACTIVE;
    }
    return ans;
  }
  
  /** @brief Gets the duration of the countdown
    * @return Countdown's duration
    */
  public int getDuration()
  {
    return mDuration;
  }
  
  public void setDuration(int duration)
  {
    mDuration = duration;
    if(mState == CountdownData.ELAPSED)
      setState(CountdownData.ACTIVE);
  }
  
  public long getStartTime()
  {
    return mStartTime;
  }
}

