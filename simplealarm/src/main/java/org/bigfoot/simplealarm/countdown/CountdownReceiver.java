/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.countdown;

import android.content.Context;
import android.content.Intent;

import org.bigfoot.simplealarm.notifiable.NotifiableReceiver;

/** @brief Broadcast receiver for the countdown notification
  */
public class CountdownReceiver extends NotifiableReceiver
{
  @Override
  protected Class getNotifierClass() {return CountdownNotifier.class;}
  
  /** @brief Called when ring event is received.
    * @param[in] context: context of the receiver
    * @param[in] intent: intent received
    */
  @Override
  public void onReceive(Context context, Intent intent)
  {
    long countdownId = getNotifiableId(intent);
    
    CountdownSQL sql = new CountdownSQL(context);
    CountdownData countdown = sql.getCountdownFromId(countdownId);
    
    // Alarm might not be in the database anymore but intent still pending
    // this migth occur if the user deleted the program data.
    if(countdown != null)
    {
      startNotificationActivity(context, intent);
    }
  }
}

