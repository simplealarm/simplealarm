/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.countdown;

import android.support.v4.app.FragmentActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.widget.LinearLayout;

import org.bigfoot.simplealarm.R;
import org.bigfoot.simplealarm.notifiable.NotifiableDetailsFragment;

/** @brief Countdown details activity*/
public class CountdownDetails extends FragmentActivity
{
  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.detailsactivity);
    
    LinearLayout container = (LinearLayout) findViewById(R.id.detailsContainer);
    container.removeAllViews();
    
    setResult(FragmentActivity.RESULT_CANCELED);
    getActionBar().setDisplayHomeAsUpEnabled(true);
    
    Intent i = getIntent();
    final long countdownId = i.getLongExtra(NotifiableDetailsFragment.NOTIFIABLE_ID, -1);
    
    Intent ans = new Intent();
    ans.putExtra(NotifiableDetailsFragment.NOTIFIABLE_ID, countdownId);
    setResult(FragmentActivity.RESULT_OK, ans);
    
    CountdownDetailsFragment details = CountdownDetailsFragment.build(countdownId);
    getSupportFragmentManager().beginTransaction().replace(R.id.detailsContainer,
        details).commit();
  }
  
  /** @brief Called when user selects an item in the options
    *
    * @param[in] item: Element selected.
    */
  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
    boolean ans = false;
    switch(item.getItemId())
    {
      case android.R.id.home:
        NavUtils.navigateUpFromSameTask(this);
        ans = true;
        break;
      default:
        break;
    }
    
    if(!ans)
    {
      ans = super.onOptionsItemSelected(item);
    }
    
    return ans;
  }  
}

