/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.views;

import android.content.Context;

import android.util.AttributeSet;
import android.support.v4.content.res.ResourcesCompat;

import org.bigfoot.simplealarm.R;
import org.bigfoot.simplealarm.alarm.AlarmData;

/** @brief Button showing the state of an alarm.
  */
public class AlarmStateButton extends MultiStateButton {
  
  /** @brief Base Constructor
    * @param[in] context: current context
    */
  public AlarmStateButton(Context context)
  {
    super(context);
  }
  
  /** @brief Base Constructor
    * @param[in] context: current context
    * @param[in] attrs: attributes to set to the button
    */
  public AlarmStateButton(Context context, AttributeSet attrs)
  {
    super(context, attrs);
  }
  
  /** @brief Base Constructor
    * @param[in] context: current context
    * @param[in] attrs: attributes to set to the button
    * @param[in] defStyle: style of the button
    */
  public AlarmStateButton(Context context, AttributeSet attrs, int defStyle)
  {
    super(context, attrs, defStyle);
  }

  /** @brief Sets the image of the button.
    * Selects the alarm corresponding to the
    * current state.
    */
  @Override
  protected void setImage(int state)
  {
    switch(state)
    {
      case AlarmData.ACTIVE:
        setImageDrawable(
            ResourcesCompat.getDrawable(getResources(), R.drawable.ic_alarm_active, null)
        );
        break;
      case AlarmData.SKIPPED:
        setImageDrawable(
            ResourcesCompat.getDrawable(getResources(), R.drawable.ic_alarm_skip, null)
        );
        break;
      case AlarmData.RINGING:
        setImageDrawable(
            ResourcesCompat.getDrawable(getResources(), R.drawable.ic_alarm_ringing, null)
        );
        break;
      case AlarmData.INACTIVE:
      default:
        setImageDrawable(
            ResourcesCompat.getDrawable(getResources(), R.drawable.ic_alarm_inactive, null)
        );
        break;
    }
  }
}
