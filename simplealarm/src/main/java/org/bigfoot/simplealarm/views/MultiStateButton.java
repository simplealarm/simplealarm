/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.views;

import android.content.Context;

import android.util.AttributeSet;

import android.widget.ImageView;

/** @brief Button showing the state of an alarm.
  */
public abstract class MultiStateButton extends ImageView {
  private int mState = 0;  ///< Current state
  
  /** @brief Base Constructor
    * @param[in] context: current context
    */
  public MultiStateButton(Context context)
  {
    super(context);
    init();
  }
  
  /** @brief Base Constructor
    * @param[in] context: current context
    * @param[in] attrs: attributes to set to the button
    */
  public MultiStateButton(Context context, AttributeSet attrs)
  {
    super(context, attrs);
    init();
  }
  
  /** @brief Base Constructor
    * @param[in] context: current context
    * @param[in] attrs: attributes to set to the button
    * @param[in] defStyle: style of the button
    */
  public MultiStateButton(Context context, AttributeSet attrs, int defStyle)
  {
    super(context, attrs, defStyle);
    init();
  }
  
  /** @brief Initializes the view.
    */
  void init()
  {
    setImage(mState);
  }
  
  /** @brief Sets the state of the button.
    * Will update the the button drawing.
    * @param[in] state: new state of the button.
    */
  public void setState(int state)
  {
    mState = checkState(state);
    setImage(mState);
  }
  
  /** @brief checks that the state is available
    *
    * @param[in] state: state to check
    * @return state to set, might be different from input
    */
  private int checkState(int state)
  {
    return state;
  }
  
  /** @brief Sets the image of the button.
    * Selects the alarm corresponding to the
    * current state.
    */
  protected abstract void setImage(int state);
}
