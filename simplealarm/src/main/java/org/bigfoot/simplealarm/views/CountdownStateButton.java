/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.views;

import android.content.Context;

import android.util.AttributeSet;
import android.support.v4.content.res.ResourcesCompat;

import org.bigfoot.simplealarm.R;
import org.bigfoot.simplealarm.countdown.CountdownData;

/** @brief Button showing the state of an alarm.
  */
public class CountdownStateButton extends MultiStateButton {
  
  /** @brief Base Constructor
    * @param[in] context: current context
    */
  public CountdownStateButton(Context context)
  {
    super(context);
  }
  
  /** @brief Base Constructor
    * @param[in] context: current context
    * @param[in] attrs: attributes to set to the button
    */
  public CountdownStateButton(Context context, AttributeSet attrs)
  {
    super(context, attrs);
  }
  
  /** @brief Base Constructor
    * @param[in] context: current context
    * @param[in] attrs: attributes to set to the button
    * @param[in] defStyle: style of the button
    */
  public CountdownStateButton(Context context, AttributeSet attrs, int defStyle)
  {
    super(context, attrs, defStyle);
  }
  
  /** @brief Initializes the view.
    */
  @Override
  protected void init()
  {
    super.init();
  }
  
  /** @brief Sets the image of the button.
    * Selects the alarm corresponding to the
    * current state.
    */
  @Override
  protected void setImage(int state)
  {
    switch(state)
    {
      case CountdownData.ACTIVE:
        setImageDrawable(
            ResourcesCompat.getDrawable(getResources(), R.drawable.ic_countdown_counting, null)
        );
        break;
      case CountdownData.ELAPSED:
        setImageDrawable(
            ResourcesCompat.getDrawable(getResources(), R.drawable.ic_countdown_elapsed, null)
        );
        break;
      case CountdownData.INACTIVE:
      default:
        setImageDrawable(
            ResourcesCompat.getDrawable(getResources(), R.drawable.ic_countdown_inactive, null)
        );
        break;
    }
  }
}
