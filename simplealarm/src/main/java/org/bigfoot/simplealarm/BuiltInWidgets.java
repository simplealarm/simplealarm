package org.bigfoot.simplealarm;

import android.content.Context;
import android.content.Intent;

import org.bigfoot.simplealarmwidgets.PluginReceiver;

public class BuiltInWidgets extends PluginReceiver {
    public BuiltInWidgets() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {}

    @Override
    public UpdaterDescriptor[] getWidgetUpdaters()
    {
        PluginReceiver.UpdaterDescriptor[] updaters = new PluginReceiver.UpdaterDescriptor[2];
        updaters[0] = new PluginReceiver.UpdaterDescriptor(
                ClockWidget.class.getName(), new ClockWidget().getName());
        updaters[1] = new PluginReceiver.UpdaterDescriptor(
                DigitalClockWidget.class.getName(), new DigitalClockWidget().getName());
        return updaters;
    }
}
