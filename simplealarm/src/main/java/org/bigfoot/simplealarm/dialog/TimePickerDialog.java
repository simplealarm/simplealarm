/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.dialog;

import android.app.AlertDialog;

import android.content.Context;

import android.widget.TimePicker;

/** @brief Dialog box for picking time.
  * This class is used instead of the android.app.TimePickerDialog which behavior
  * changed from one android version to the other, making it complicated to handle.
  */
public class TimePickerDialog extends AlertDialog 
{
  private TimePicker mTimePicker = null;  ///< time picker view
  
  /** Base Constructor
    * @param[in] context: current context
    * @param[in] hour: default hour to display
    * @param[in] min: default minute to display
    * @param[in] is24HourFormat: selects the 12 or 24 hour format
    * @param[in] title: title of the dialog box
    */
  TimePickerDialog(Context context, int hour, int min, boolean is24HourFormat, String title)
  {
    super(context);
    if(title != null)
    {
      setTitle(title);
    }
    setTimePicker(context, hour, min, is24HourFormat);
  }
  
  /** @brief Creates the time picker view
    * @param[in] context: current context
    * @param[in] hour: hour to display
    * @param[in] min: minute to display
    * @param[in] is24HourFormat: selects the 12 or 24 hour format*/
  private void setTimePicker(Context context, int hour, int min, boolean is24HourFormat)
  {
    mTimePicker = new TimePicker(context);
    
    mTimePicker.setIs24HourView(is24HourFormat);
    mTimePicker.setCurrentHour(hour);
    mTimePicker.setCurrentMinute(min);
    
    setView(mTimePicker);
  }
  
  /** @brief get the hour selected by the user
    * @return the hour selected
    */
  public int getHour()
  {
    return mTimePicker.getCurrentHour();
  }
  
  /** @brief get the minute selected by the user
    * @return the minute selected
    */
  public int getMinute()
  {
    return mTimePicker.getCurrentMinute();
  }
}
