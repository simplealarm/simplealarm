/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.dialog;

import android.app.AlertDialog;

import android.content.Context;

import android.widget.EditText;

/** @brief Dialog box for picking text.
  */
public class TextPickerDialog extends AlertDialog 
{
  private EditText mTextPicker = null;  ///< time picker view
  
  /** Base Constructor
    * @param[in] context: current context
    * @param[in] text: string to pick
    * @param[in] title: title of the dialog box
    */
  TextPickerDialog(Context context, String text, String title)
  {
    super(context);
    if(title != null)
    {
      setTitle(title);
    }
    setTextPicker(context, text);
  }
  
  /** @brief Creates the time picker view
    * @param[in] context: current context
    * @param[in] text: text value
    */
  private void setTextPicker(Context context, String text)
  {
    mTextPicker = new EditText(context);
    mTextPicker.setText(text);
    
    setView(mTextPicker);
  }
  
  /** @brief get the hour selected by the user
    * @return the hour selected
    */
  public String getText()
  {
    return mTextPicker.getText().toString();
  }
}
