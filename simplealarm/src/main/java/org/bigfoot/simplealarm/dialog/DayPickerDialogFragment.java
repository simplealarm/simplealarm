/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.dialog;

import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import android.os.Bundle;

/** @brief Dialog fragment for day selection.
  */
public class DayPickerDialogFragment extends DialogFragment
{
  private DayPickerDialog.OnValidateListener mOnValidate = null;  ///< On validation callback.
  private boolean[] mDaysSelection = new boolean[8]; ///< list of selected days
  /**< Stores 8 elements because week days start from 1 to 7.*/

  /** @brief Constructs the day picker dialog fragment
    * @param[in] daysSelection: input days selection
    * @param[in] onValidate: on validation callback
    * @return A new day picker dialog fragment.
    */
  public static DayPickerDialogFragment build(
      boolean[] daysSelection,
      DayPickerDialog.OnValidateListener onValidate)
  {
    DayPickerDialogFragment f = new DayPickerDialogFragment();
    f.mOnValidate = onValidate;
    f.mDaysSelection = daysSelection;
    return f;
  }
  
  @Override
  @NonNull
  /** @brief Creates the day picker dialog box
    * @param[in] savedInstanceState: saved instance state
    * @return Day Picker Dialog Box.
    */
  public Dialog onCreateDialog(Bundle savedInstanceState)
  {
    return new DayPickerDialog(getActivity(), mDaysSelection, mOnValidate);
  }
}
