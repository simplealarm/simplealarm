/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.ListView;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import org.bigfoot.simplealarm.common.CheckAdapter;
import org.bigfoot.simplealarm.common.CheckElement;

/** @brief Dialog box for day selection.
  */
public class DayPickerDialog extends AlertDialog
{
  /// Base class for validation listener
  public interface OnValidateListener
  {
    /** Called on user validation.
      * @param[in] daysSelection: selection of the days.
      */
    void onValidate(boolean[] daysSelection);
  }
  
  /** @brief Day item.
    */
  private class DayElement extends CheckElement<Integer>
  {
    public DayElement(boolean selected, Integer value) {super(selected, value);}
    @Override
    public String getDescription() {return new DateFormatSymbols().getWeekdays()[mValue];}
  }
  
  //public static final String DAYS_SELECTION_EXTRA = "org.bigfoot.simplealarm.DaysSelection";
  private static final int nDays = 8; ///< maximum number of days, 8 because android counts days from 1 to 8
  
  private OnValidateListener mOnValidate = null; ///< On validation callback.
  private CheckAdapter mAdapter = null; ///< Adapter to display the day selection.
  
  /** Base Constructor
    * @param[in] context: current context
    * @param[in] daysSelection: input days selection
    * @param[in] onValidate: on validation callback
    */
  DayPickerDialog(Context context, boolean[] daysSelection, OnValidateListener onValidate)
  {
    super(context);
    mOnValidate = onValidate;
    
    setButton(DialogInterface.BUTTON_POSITIVE, context.getResources().getString(android.R.string.ok),
      new DialogInterface.OnClickListener(){
        @Override
        public void onClick(DialogInterface d, int which) { validate(); }
      });

    mAdapter = new CheckAdapter(context, new ArrayList<CheckElement>());
    ListView lvDays = new ListView(context);
    
    lvDays.setAdapter(mAdapter);
    
    
    int maxDays = Math.min(daysSelection.length, nDays);
    for(int i = 0; i < nDays; ++i)
    {
      if(i > 0)
      {
        boolean selected = false;
        if(i < maxDays){
          selected = daysSelection[i];
        }
        mAdapter.add(new DayElement(selected, i));
      }
    }
    setView(lvDays);
  }
  
  /** @brief called when the user leaves the dialog box.*/
  private void validate()
  {
    if(mOnValidate != null)
    {
      boolean[] daysSelection = new boolean[nDays];
      DayElement day;
      for(int i = 0; i < 7; ++i)
      {
        day = (DayElement) mAdapter.getItem(i);
        daysSelection[day.getValue()] = day.getSelected();
      }
      
      mOnValidate.onValidate(daysSelection);
    }
  }
 
  /** Helper function to return a text with the selected days short names
    * @param[in] daysSelection: array of selected days.
    * @param[in] sep: separator to use between days names
    * @return String with the list of active days.
    */ 
  public static String getShortDays(boolean[] daysSelection, String sep)
  {
    String daysText = "";
    String[] daysList = new DateFormatSymbols().getShortWeekdays();
    
    int maxDays = Math.min(DayPickerDialog.nDays, daysSelection.length);
    
    for(int i = 0; i < maxDays; ++i)
    {
      if(i>0)
      {
        if(daysSelection[i])
        {
          daysText += daysList[i];
          daysText += sep;
        }
      }
    }
    return daysText;
  }
}
