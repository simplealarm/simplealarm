/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.dialog;

import android.app.AlertDialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;

import org.bigfoot.simplealarm.R;

/** @brief Dialog box for picking time.
  * This class is used instead of the android.app.TimePickerDialog which behavior
  * changed from one android version to the other, making it complicated to handle.
  */
public class DurationPickerDialog extends AlertDialog 
{
  private NumberPicker mSecondsPicker = null;  ///< seconds picker view
  private NumberPicker mMinutesPicker = null;  ///< minutes picker view
  private NumberPicker mHoursPicker = null;    ///< hours picker view

  
  /** Base Constructor
    * @param[in] context: current context
    * @param[in] duration: duration in seconds
    * @param[in] title: title of the dialog box
    */
  DurationPickerDialog(Context context, int duration, String title)
  {
    super(context);
    if(title != null)
    {
      setTitle(title);
    }
    int hours = duration / 3600;
    int rest  = duration % 3600;
    int minutes = rest / 60;
    int seconds = rest % 60;
    setDurationPicker(context, hours, minutes, seconds);
  }
  
  /** @brief Creates the time picker view
    * @param[in] context: current context
    * @param[in] hours: default hours to display
    * @param[in] minutes: default minutes to display
    * @param[in] seconds: default seconds to display*/
  private void setDurationPicker(Context context, int hours, int minutes, int seconds)
  {
    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.durationpickerdialog, null);
  
    mHoursPicker   = (NumberPicker) view.findViewById(R.id.hoursPicker);
    mMinutesPicker = (NumberPicker) view.findViewById(R.id.minutesPicker);
    mSecondsPicker = (NumberPicker) view.findViewById(R.id.secondsPicker);
    
    configNumberPicker(mHoursPicker, hours, 99);
    configNumberPicker(mMinutesPicker, minutes, 59);
    configNumberPicker(mSecondsPicker, seconds, 59);
    
    setView(view);
  }
  
  private void configNumberPicker(NumberPicker numberPicker, int value, int max)
  {
    numberPicker.setMinValue(0);
    numberPicker.setMaxValue(max);
    numberPicker.setValue(value);
    numberPicker.setWrapSelectorWheel(true);
  }

  
  /** @brief Get the duration selected by the user in seconds.
    * @return duration
    */
  public int getDuration()
  {
    int seconds = mHoursPicker.getValue() * 3600;
    seconds += mMinutesPicker.getValue() * 60;
    seconds += mSecondsPicker.getValue();
    return seconds;
  }
}
