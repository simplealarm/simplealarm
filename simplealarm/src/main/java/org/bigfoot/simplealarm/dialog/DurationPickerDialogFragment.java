/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.dialog;

import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import android.content.DialogInterface;

import android.os.Bundle;

/** Fragment for the TimePickerDialog*/
public class DurationPickerDialogFragment extends DialogFragment {
  private int mDuration = 60;  ///< Duration to pass as argument to the dialog box.
  private String mTitle = null;///< Title to pass as argument to the dialog box.
  private DialogInterface.OnClickListener mOnValidate = null; ///< On validate listener.
  
  /** @brief Dialog builder for the time picker box
    * @param[in] duration: default duration in seconds to display
    * @param[in] title: title of the dialog box
    * @param[in] onValidate: validation listener
    * @return Time Picker Dialog fragment
    */
  public static DurationPickerDialogFragment build(int duration, String title,
    DialogInterface.OnClickListener onValidate) {
    DurationPickerDialogFragment f = new DurationPickerDialogFragment();
    f.mDuration = duration;
    f.mTitle = title;
    f.mOnValidate = onValidate;
    return f;
  }
  
  @Override
  @NonNull
  /** @brief Creates the time picker dialog box
    * @param[in] savedInstanceState: saved instance state
    * @return dialog box
    */
  public Dialog onCreateDialog(Bundle savedInstanceState)
  {
    DurationPickerDialog d = new DurationPickerDialog(getActivity(), mDuration, mTitle);
    d.setButton(DialogInterface.BUTTON_POSITIVE, getResources().getString(android.R.string.ok),
        mOnValidate);
    return d;
  }
}
