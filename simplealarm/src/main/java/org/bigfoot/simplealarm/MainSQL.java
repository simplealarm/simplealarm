/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm;

import android.content.Context;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.bigfoot.simplealarm.alarm.AlarmSQL;
import org.bigfoot.simplealarm.countdown.CountdownSQL;
import org.bigfoot.simplealarm.widget.WidgetsSQL;

/** @brief Database handler
  */
public class MainSQL extends SQLiteOpenHelper {
  private static final String  dataBaseName    = "SimpleAlarm.db";///< Name of the database
  private static final int     dataBaseVersion = 5; ///< Version of the database
  private final Context mContext;
  
  /** Base Constructor */
  public MainSQL(Context context) {
    super(context, dataBaseName, null, dataBaseVersion);
    mContext = context;
  }
  
  /** Creates the database structure
    * @param[in,out] db: handler to the database to create.*/
  @Override
  public void onCreate(SQLiteDatabase db) {
    AlarmSQL.onCreate(db);
    CountdownSQL.onCreate(db);
    WidgetsSQL.onCreate(db, mContext);
  }
  
  /** Updates the database to the newest version
    * @param[in] db: handler to the database to update
    * @param[in] oldVersion: Current version of the database
    * @param[in] newVersion: newest version of the database
    */
  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    AlarmSQL.onUpgrade(db, oldVersion, newVersion);
    CountdownSQL.onUpgrade(db, oldVersion, newVersion);
    WidgetsSQL.onUpgrade(db, oldVersion, newVersion, mContext);
  }
}
