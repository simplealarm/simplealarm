/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.pluginmanager;

import org.bigfoot.simplealarmwidgets.PluginReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.bigfoot.simplealarm.widget.WidgetsSQL;

/**This receiver is created when a package is installed/removed/replaced to check whether this
 * package is a plugin for SimpleAlarm. If the new package is a plugin, the SimpleAlarm plugin
 * database is updated.
 */
public class PluginInstallReceiver extends BroadcastReceiver
{
  /** Called on package installation to find plugins
    * @note: Each time a package is installed, look through all packages to find
    * broadcast receiver filtering on "org.bigfoot.simplealarm.EXTERNAL_WIDGET".
    * It would be better to only look at the newly installed package, but don't
    * know how.
    */
  @Override
  public void onReceive(Context ctx, Intent intent)
  { 
    // Checks whether the widget updater list must be updated
    if(willUpdate(intent))
    {
      updatePlugin(intent, ctx);
    }
  }
  
  private boolean willUpdate(Intent intent)
  {
    String action = intent.getAction();
    if(action.equals(Intent.ACTION_PACKAGE_REPLACED))
    {
      return true;
    }
    boolean replaced = intent.getBooleanExtra(Intent.EXTRA_REPLACING, false);
    // Waits for the ACTION_PACKAGE_REPLACED intent to be sent
    if(replaced)
    {
      return false;
    }
    return action.equals(Intent.ACTION_PACKAGE_ADDED)||action.equals(Intent.ACTION_PACKAGE_REMOVED);
  }
  
  /** Update the widget updater list
    * @param[in] intent: action package added/removed/replaced.
    * @param[in] context: context used to update the database.
    */
  private void updatePlugin(Intent intent, Context context)
  { 
    // Gets the newly installed package name
    String[] items = intent.getDataString().split(":");
    if(items.length > 1)
    {
      WidgetsSQL db = new WidgetsSQL(context);
      String packageName = items[1];
      if(intent.getAction().equals(Intent.ACTION_PACKAGE_REMOVED))
      {
        db.removePlugin(packageName);
      }
      else
      {
        PluginReceiver receiver = new PluginManager(context).getReceiverForPackage(packageName);
        if(receiver != null)
        {
          db.addPlugin(packageName, receiver.getWidgetUpdaters());
        }
      }
    }
  }
}

