/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.pluginmanager;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.Pair;

import org.bigfoot.simplealarmwidgets.BaseWidgetUpdater;
import org.bigfoot.simplealarmwidgets.PluginReceiver;

import java.util.ArrayList;
import java.util.List;

import dalvik.system.PathClassLoader;

/** Manages plugins.
 *
 */
public class PluginManager {
    private final Context mContext;
    private final PackageManager mPackageManager;

    public PluginManager(Context ctx)
    {
        mContext = ctx;
        mPackageManager = ctx.getPackageManager();
    }

    /**Gets the installed package with a broadcast receiver filtering the intent
     * BaseWidgetUpdater.INTENT_FILTER.
     *
     * @param ctx: input Context.
     * @return List of installed plugins.
     */
    private List<ResolveInfo> getInstalledPlugins(){
        return mPackageManager.queryBroadcastReceivers(
                new Intent(BaseWidgetUpdater.INTENT_FILTER), 0);
    }

    private ResolveInfo getInfoForPackage(List<ResolveInfo> rInfoList, String packageName)
    {
        ResolveInfo ans = null;
        for(ResolveInfo rInfo : rInfoList)
        {
            if(rInfo.activityInfo.packageName.equals(packageName))
            {
                ans = rInfo;
                break;
            }
        }
        return ans;
    }

    /** Creates a class loader for the given application with the provided context.
     * @param[in] context: context providing the "org.bigfoot.simplealarmwidgets" classes
     * @param[in] appInfo: Application info of the plugin
     * @return A classloader for the plugin.
     */
    private ClassLoader getPluginClassLoader(ApplicationInfo appInfo, Context context)
    {
        return new PathClassLoader(appInfo.sourceDir, context.getClassLoader());
    }

    /**Builds the plugin receiver corresponding to the input description.
     *
     * @param receiverInfo: BroadcastReceiver information.
     * @param appInfo: Application information of the package containing the BroadcastReceiver.
     * @return
     */
    @Nullable
    private PluginReceiver makePluginReceiver(ResolveInfo receiverInfo, ApplicationInfo appInfo)
    {
        PluginReceiver receiver = null;
        String receiverName = receiverInfo.activityInfo.name;

        try
        {
            // Loads the PluginReceiver
            Class<?> cl = Class.forName(receiverName, true, getPluginClassLoader(appInfo, mContext));
            receiver = (PluginReceiver) cl.newInstance();
        }
        catch(Exception ex)
        {
            Log.e("SimpleAlarm", "- Exception", ex);
        }
        return receiver;
    }

    /**Gets the PluginReceiver for the given package.
     *
     * @param packageName
     * @return PluginReceiver for the given package
     */
    @Nullable
    public PluginReceiver getReceiverForPackage(String packageName)
    {
        ApplicationInfo appInfo = null;
        PluginReceiver receiver = null;
        ResolveInfo rInfo = getInfoForPackage(getInstalledPlugins(), packageName);
        if(rInfo != null)
        {
          try {
              appInfo = mPackageManager.getApplicationInfo(packageName, 0);
          }
          catch(PackageManager.NameNotFoundException ex)
          {
            //Log.i("SimpleAlarm", "- Exception", ex);
          }
        }
        if(appInfo != null)
        {
          receiver = makePluginReceiver(rInfo, appInfo);
        }
        return receiver;
    }

    public List<Pair<String, PluginReceiver>> getAllPlugins()
    {
        List<Pair<String, PluginReceiver>> receivers = new ArrayList<>();
        for(ResolveInfo rInfo : getInstalledPlugins())
        {
            try {
                String pName = rInfo.activityInfo.packageName;
                ApplicationInfo appInfo = mPackageManager.getApplicationInfo(pName, 0);
                receivers.add(new Pair<>(pName, makePluginReceiver(rInfo, appInfo)));
            }
            catch(PackageManager.NameNotFoundException ex)
            {
                Log.w("SimpleAlarm", "- Exception", ex);
                Log.w("SimpleAlarm",String.format("PackageName: %s", rInfo.activityInfo.packageName));
            }
        }
        return receivers;
    }
}
