/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm;

import java.util.Calendar;
import android.content.Context;
import android.widget.RemoteViews;

import org.bigfoot.simplealarm.R;
import org.bigfoot.simplealarmwidgets.BaseWidgetUpdater;

public class ClockWidget extends BaseWidgetUpdater
{
  public ClockWidget()
  {
    this.mName = "Analog Clock";
  }
  
  /** Updates the given views*/
  public RemoteViews getViews(Context context, int appWidgetId, long now)
  {
    Calendar c = Calendar.getInstance();
    
    String date = android.text.format.DateFormat.getDateFormat(
      context.getApplicationContext()).getDateInstance(
      java.text.DateFormat.SHORT).format(c.getTime());
      
    RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.clockwidget);
    
    views.setTextViewText(R.id.date, date);
    return views;
  }
  
  public int getClickableView() {return R.id.clock;}
  
  /** Register the next widget update in the alarm manager*/
  public long getNextUpdateMillis(Context context, int appWidgetId)
  {
    Calendar c = Calendar.getInstance();
    c.add(Calendar.DAY_OF_WEEK, 1);
    c.set(Calendar.HOUR_OF_DAY, 0);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MILLISECOND, 0);
    return c.getTimeInMillis();
  }
}

