/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm;

import java.util.Calendar;

import android.content.Context;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

import android.widget.RemoteViews;

import org.bigfoot.simplealarm.R;
import org.bigfoot.simplealarmwidgets.BaseWidgetUpdater;

public class DigitalClockWidget extends BaseWidgetUpdater
{ 
  private long mLastUpdate = 0;
  private String mDate = "";
  private String mTime = "";
  
  public DigitalClockWidget()
  {
    this.mName = "Digital Clock";
  }
  
  /** Updates the given views*/
  public RemoteViews getViews(Context context, int appWidgetId, long now)
  {
    RemoteViews views;
    
    if(mLastUpdate != now)
    {
      mLastUpdate = now;
      Calendar c = Calendar.getInstance();
      mDate = android.text.format.DateFormat.getDateFormat(
        context.getApplicationContext()).getDateInstance(
        java.text.DateFormat.SHORT).format(c.getTime());
        
      mTime = android.text.format.DateFormat.getTimeFormat(
        context.getApplicationContext()).getTimeInstance(
        java.text.DateFormat.SHORT).format(c.getTime());
    }
    views = new RemoteViews(context.getPackageName(), R.layout.digitalclockwidget);
    views.setImageViewBitmap(R.id.clockBitMap, makeDateTime(context, mTime, mDate));
    
    return views;
  }
  
  public int getClickableView() {return R.id.clockBitMap;}
  
  /** Register the next widget update in the alarm manager*/
  public long getNextUpdateMillis(Context context, int appWidgetId)
  {
    Calendar c = Calendar.getInstance();
    c.add(Calendar.MINUTE, 1);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MILLISECOND, 0);
    return c.getTimeInMillis();
  }
  
  private Bitmap makeDateTime(Context context, String time, String date)
  {
    Bitmap clockBitMap = Bitmap.createBitmap(200, 120, Bitmap.Config.ARGB_8888);
    Canvas clockCanvas = new Canvas(clockBitMap);
    Paint paint = new Paint();
    Typeface gentiusFont = Typeface.createFromAsset(context.getAssets(), "fonts/Gentius.ttf");
    paint.setAntiAlias(true);
    paint.setSubpixelText(true);
    paint.setTypeface(gentiusFont);
    paint.setStyle(Paint.Style.FILL);
    paint.setColor(Color.WHITE);
    paint.setTextSize(95);
    paint.setTextAlign(Paint.Align.CENTER);
    clockCanvas.drawText(time, 100, 65, paint);
    paint.setTextSize(32);
    clockCanvas.drawText(date, 100, 100, paint);
    return clockBitMap;
  }
}

