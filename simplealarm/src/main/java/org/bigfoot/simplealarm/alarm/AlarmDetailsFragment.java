/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.alarm;

import android.content.DialogInterface;

import android.os.Bundle;

import android.text.format.DateFormat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import org.bigfoot.simplealarm.R;
import org.bigfoot.simplealarm.dialog.DayPickerDialog;
import org.bigfoot.simplealarm.dialog.DayPickerDialogFragment;
import org.bigfoot.simplealarm.dialog.TimePickerDialog;
import org.bigfoot.simplealarm.dialog.TimePickerDialogFragment;
import org.bigfoot.simplealarm.notifiable.NotifiableData;
import org.bigfoot.simplealarm.notifiable.NotifiableDetailsFragment;
import org.bigfoot.simplealarm.views.AlarmStateButton;

/** @brief Fragment displaying the details of an alarm.*/
public class AlarmDetailsFragment extends NotifiableDetailsFragment
{
  private AlarmSQL    mAlarmSQL = null; ///< Database containing the alarms
  private AlarmData   mAlarm = null;    ///< Alarm parameters
  private AlarmStateButton mAlarmState = null;  ///< Button to select the alarm state
  private TextView    mAlarmStateTxt= null; ///< Displays the alarm state as text
  private TextView    mAlarmTime = null;  ///< Displays the alarm time
  private TextView    mAlarmDays = null;  ///< Displays the alarm days
  
  /** @brief Builder
    * @param[in] notifiableId: Identifier of the alarm
    * @return Returns the newl created AlarmDetailsFragment.
    */
  public static AlarmDetailsFragment build(long notifiableId)
  {
    AlarmDetailsFragment f = new AlarmDetailsFragment();
    Bundle args = new Bundle();
    args.putLong(NOTIFIABLE_ID, notifiableId);
    f.setArguments(args);
    return f;
  }
  
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  { 
    mAlarmSQL = new AlarmSQL(getActivity());
    return super.onCreateView(inflater, container, savedInstanceState);
  }
  
  @Override
  protected View setContent(LayoutInflater inflater, ViewGroup container)
  {
    View v = inflater.inflate(R.layout.alarmdetailsfragment, container, false);
    
    mAlarmTime = (TextView) v.findViewById(R.id.alarmDetailsTime);
    mAlarmState= (AlarmStateButton) v.findViewById(R.id.alarmDetailsState);
    mAlarmStateTxt= (TextView) v.findViewById(R.id.alarmDetailsStateHint);
    mAlarmDays = (TextView) v.findViewById(R.id.alarmDetailsRepeat);
    
    // set listener
    mAlarmTime.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        selectRingTime(v);
      }
    });
    
    mAlarmState.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        nextState();
      }
    });
    
    v.findViewById(R.id.alarmDetailsRepeatLayout).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        selectRepeat(v);
      }
    });
    return v;
  }
  
  @Override
  protected NotifiableData makeNotifiable(long alarmId)
  {
    return mAlarmSQL.getAlarmFromId(alarmId);
  }
  
  @Override
  protected void saveChanges()
  {
    mAlarmSQL.setAlarm(mAlarm);
  }
  
  /** @brief Updates the GUI
    * @param[in] alarmId: Id of the alarm to use
    */
  @Override
  public void updateGUI(long alarmId)
  {
    super.updateGUI(alarmId);
    if(alarmId >= 0)
    {
      mAlarm = (AlarmData) getNotifiable();
      setState(mAlarm.getState());
      setGUITime();
      setGUIRepeat(mAlarm.getDays());
    }
  }
  
  /** @brief Prompt the user for a ring time
    * @param[in] v: view used to select the ringtime
    */
  private void selectRingTime(View v)
  {
    String pickerTitle = mAlarm.getName();
    int hour  = mAlarm.getHour();
    int min   = mAlarm.getMinute();
    
    TimePickerDialogFragment tpdf = TimePickerDialogFragment.build(
      hour, min, DateFormat.is24HourFormat(getActivity()), pickerTitle,
      new DialogInterface.OnClickListener(){
        @Override
        public void onClick(DialogInterface d, int which) {
          int hour = ((TimePickerDialog) d).getHour();
          int minute = ((TimePickerDialog) d).getMinute();
          mAlarm.setTime(hour, minute);
          saveChanges();
          setGUITime();
        }
      });
    tpdf.show(getFragmentManager(), "timePickerDialog");
  }
  
  /** @brief Sets the alarm to its next state */
  private void nextState()
  {
    int state = mAlarm.nextState();
    mAlarm.setState(state);
    saveChanges();
    setState(state);
  }
  
  /** Update the state view depending on the input state
   *  @param[in] state: state of the alarm */
  private void setState(int state)
  {
    int textId;
    switch(state)
    {
      case AlarmData.ACTIVE:
        textId = R.string.alarm_active;
        break;
      case AlarmData.SKIPPED:
        textId = R.string.alarm_skipped;
        break;
      case AlarmData.RINGING:
        textId = R.string.alarm_ringing;
        break;
      case AlarmData.INACTIVE:
      default:
        textId = R.string.alarm_inactive;
        break;
    }
    mAlarmState.setState(state);
    mAlarmStateTxt.setText(getResources().getString(textId));
  }
  
  /** Called when users ask for a repeat days selection
    * @param[in] v: view used
    */  
  private void selectRepeat(View v)
  {
    DayPickerDialogFragment dpdf = DayPickerDialogFragment.build(
      mAlarm.getDays(),
      new DayPickerDialog.OnValidateListener(){
        @Override
        public void onValidate(boolean[] daysSelection) {
          mAlarm.setDays(daysSelection);
          saveChanges();
          setGUIRepeat(daysSelection);
        }
      });
    dpdf.show(getFragmentManager(), "timePickerDialog");
  }
  
  /** @brief Displays time as text*/
  private void setGUITime()
  {
    mAlarmTime.setText(mAlarm.getTimeText(getActivity()));
  }
  
  /** @brief Repeat days as text*/
  private void setGUIRepeat(boolean[] days)
  {
    mAlarmDays.setText(DayPickerDialog.getShortDays(days, " "));
  }
}
