/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.alarm;

import android.content.ContentValues;
import android.content.Context;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import android.net.Uri;

import org.bigfoot.simplealarm.MainSQL;

public class AlarmSQL {
  private static final String ALARM_TABLE    = "Alarms";  ///< name of the alarm table
  private static final String ID_COLUMN      = "_id";      ///< name of the private key field
  private static final String NAME_COLUMN    = "name";     ///< name of the alarm name field
  private static final String HOUR_COLUMN    = "hour";     ///< name of the hour field
  private static final String MINUTE_COLUMN  = "minute";   ///< name of the minute field
  private static final String REPEAT_COLUMN  = "repeat";   ///< name of the repeat field
  private static final String ACTIVE_COLUMN  = "active";   ///< name of the active field
  private static final String RINGURI_COLUMN = "ringUri";  ///< name of the ring uri field

  private MainSQL mMainSQL = null;  ///< main database
  private AlarmRegisterer mAlarmRegisterer = null;  ///< alarm registerer
  
  /** @brief Base Constructor
    * @param[in] context: current context
    */
  public AlarmSQL(Context context)
  {
    mMainSQL = new MainSQL(context);
    mAlarmRegisterer = new AlarmRegisterer(context);
  }
  
  /** @brief Called on the database creation
    * @param[in] db: Database to modify
    */
  public static void onCreate(SQLiteDatabase db)
  {
    final String create = "create table " + ALARM_TABLE + "(" + 
      ID_COLUMN     + " integer primary key autoincrement, " +
      NAME_COLUMN   + " text not null, " + 
      HOUR_COLUMN   + " integer, " +
      MINUTE_COLUMN + " integer, " +
      REPEAT_COLUMN + " integer, " +
      ACTIVE_COLUMN + " integer, " +
      RINGURI_COLUMN+ " text"      + ");";
    db.execSQL(create);
  }
  
  /** @brief Called on database upgrade
    * 
    * This function performs update on the alarm table from a version to the other.
    * @param[in] db: Database to upgrade
    * @param[in] oldVersion: Old version of the database
    * @param[in] newVersion: version to which the database must be upgrade
    */
  public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
  {
    switch(oldVersion)
    {
      case 1:
      case 2:
      default:
        break;
    }
  }
  
  /** Returns the alarm corresponding to the given Id.
    * @param[in] id: id of the alarm to find.
    * @return The alarm corresponding to the id.
    */
  public AlarmData getAlarmFromId(long id)
  {
    // Query to selection the alarm in the database
    final String selQuery = String.format("%s = %d", ID_COLUMN, id);
    SQLiteDatabase db = open();
    Cursor alarmCursor    = db.query(ALARM_TABLE, null,
      selQuery , null,
      null, null,
      ID_COLUMN, null);
      
    // Colums index
    final int nameIndex    = alarmCursor.getColumnIndex(NAME_COLUMN);
    final int activeIndex  = alarmCursor.getColumnIndex(ACTIVE_COLUMN);
    final int hourIndex    = alarmCursor.getColumnIndex(HOUR_COLUMN);
    final int minuteIndex  = alarmCursor.getColumnIndex(MINUTE_COLUMN);
    final int repeatIndex  = alarmCursor.getColumnIndex(REPEAT_COLUMN);
    final int ringUriIndex = alarmCursor.getColumnIndex(RINGURI_COLUMN);
    
    // Move to the first element (might not be required)
    alarmCursor.moveToFirst();
    
    // ringUri
    Uri ringUri = null;
    String stringUri = alarmCursor.getString(ringUriIndex);
    
    if(stringUri.length() != 0)
    {
      ringUri = Uri.parse(stringUri);
    }
    
    // Make the AlarmData from the database
    AlarmData alarm = new AlarmData(alarmCursor.getString(nameIndex),
      alarmCursor.getInt(activeIndex), alarmCursor.getInt(hourIndex),
      alarmCursor.getInt(minuteIndex), alarmCursor.getInt(repeatIndex));
    alarm.setId(id);
    alarm.setRingUri(ringUri);

    alarmCursor.close();
    db.close();
    return alarm;
  }
  
  /** Returns the list of alarms in the database
   *  @return Array containing the alarms
   */ 
  public AlarmData[] getAlarms()
  {
    String[] columns = new String[1];
    columns[0] = ID_COLUMN;
    SQLiteDatabase db = open();
    Cursor alarmsCursor = db.query(ALARM_TABLE, columns,
      null, null,
      null, null,
      ID_COLUMN, null);
    final int nAlarms  = alarmsCursor.getCount();
    final int idsIndex = alarmsCursor.getColumnIndex(ID_COLUMN);
    AlarmData[] alarmsArray = new AlarmData[nAlarms];
    
    alarmsCursor.moveToFirst();
    for(int i = 0; i < nAlarms; ++i)
    {
      alarmsArray[i] = getAlarmFromId(alarmsCursor.getLong(idsIndex));
      alarmsCursor.moveToNext();
    }
    alarmsCursor.close();
    db.close();
    return alarmsArray;
  }
  
  /** @brief Register all active alarms on device boot */
  public void onBootRegister()
  {
    SQLiteDatabase db = open();
    String[] columns = new String[1];
    columns[0] = ID_COLUMN;
    Cursor alarmsCursor = db.query(ALARM_TABLE, columns,
      String.format("%s != %s",ACTIVE_COLUMN, AlarmData.INACTIVE), null,
      null, null,
      ID_COLUMN, null);
    
    final int idsIndex = alarmsCursor.getColumnIndex(ID_COLUMN);
    
    alarmsCursor.moveToFirst();
    for(int i = 0; i < alarmsCursor.getCount(); ++i)
    {
      AlarmData alarm = getAlarmFromId(alarmsCursor.getLong(idsIndex));
      mAlarmRegisterer.setAlarm(alarm);
      alarmsCursor.moveToNext();
    }
    alarmsCursor.close();
    db.close();
  }
  
  /** @brief Open the database
    * @return The opened SQLite database.
    */
  private SQLiteDatabase open() throws SQLException
  {
    return mMainSQL.getWritableDatabase();
  }

  /** @brief Adds an alarm to the database and arm it if needed
    * @param[in] alarm: alarm to add
    * @return the time in milliseconds at which the alarm will ring
    */
  public long setAlarm(AlarmData alarm)
  {
    updateDataBase(alarm);
    return mAlarmRegisterer.setAlarm(alarm);
  }
  
  /** @brief Removes the alarm.
    * 
    * Firstly unregister the alarm, then deletes it from the daatbase.
    * @param[in] alarm: alarm to add
    */
  public void delAlarm(AlarmData alarm)
  {
    alarm.setState(AlarmData.INACTIVE);
    mAlarmRegisterer.setAlarm(alarm);
    SQLiteDatabase db = open();
    db.delete(ALARM_TABLE, String.format("%s = %d", ID_COLUMN, alarm.getId()), null);
    db.close();
    alarm.setId(-1);
  }
  
  /** @brief Updates the database with the alarm
    * 
    * Adds or replace the alarm in the database
    * @param[in] alarm: alarm to add or update
    */
  private void updateDataBase(AlarmData alarm)
  {
    // Update the database
    Uri ringUri = alarm.getRingUri();
    String ringString = "";
    if(ringUri != null)
    {
      ringString = ringUri.toString();
    }
    ContentValues values = new ContentValues();
    values.put(NAME_COLUMN  ,  alarm.getName());
    values.put(HOUR_COLUMN  ,  alarm.getHour());
    values.put(MINUTE_COLUMN,  alarm.getMinute());
    values.put(REPEAT_COLUMN,  alarm.getRepeat());
    values.put(ACTIVE_COLUMN,  alarm.getState());
    values.put(RINGURI_COLUMN, ringString);
    long id = alarm.getId();
    
    SQLiteDatabase db = open();
    // Adds the alarm to the data base...
    if(id == -1)
    {
      alarm.setId(db.insert(ALARM_TABLE, null, values));
    }
    // ... or update the existing entry
    else
    {
      db.update(ALARM_TABLE, values, String.format("%s = %d", ID_COLUMN, id), null);
    }
    db.close();
  }
  
  /** @brief Delays the alarm from few minutes
    * @param[in] alarm: alarm to delay
    */
  public void delayAlarm(AlarmData alarm)
  {
    alarm.setState(AlarmData.RINGING);
    updateDataBase(alarm);
    mAlarmRegisterer.delayAlarm(alarm);
  }
  
  /** @brief Rearms the alarm.
    * 
    * Called after an alarm notification. Will rearm the alarm if required
    * @param[in] alarm: alarm to rearm
    */
  public void alarmNotified(AlarmData alarm)
  {
    // If alarm is not set to repeat, cancel it
    if(alarm.getRepeat() == 0)
    {
      alarm.deactivate();
    }
    else
    {
      alarm.activate();
    }
    updateDataBase(alarm);
    mAlarmRegisterer.setAlarm(alarm);
  }
}

