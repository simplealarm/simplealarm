/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.alarm;

import android.content.Context;
import android.content.Intent;
import org.bigfoot.simplealarm.notifiable.NotifiableReceiver;

/** @brief Broadcast receiver for the alarm notifications
  *
  * This class is in charge for handling the alarm events:
  * - skipping
  * - ringing
  * - delaying
  */
public class AlarmReceiver extends NotifiableReceiver
{
  @Override
  protected Class getNotifierClass() {return AlarmNotifier.class;}
  
  /** @brief Called when ring event is received.
    * @param[in] context: context of the receiver
    * @param[in] intent: intent received
    */
  @Override
  public void onReceive(Context context, Intent intent)
  {
    long alarmId = getNotifiableId(intent);
    
    AlarmSQL alarmSQL = new AlarmSQL(context);
    AlarmData alarm = alarmSQL.getAlarmFromId(alarmId);
    
    // Alarm might not be in the database anymore but intent still pending
    // this migth occur if the user deleted the program data.
    if(alarm != null)
    {
      if(alarm.getState() == AlarmData.SKIPPED)
      {
        alarm.setState(AlarmData.ACTIVE);
        alarmSQL.alarmNotified(alarm);
      }
      else
      {
        startNotificationActivity(context, intent);
      }
    }
  }
}

