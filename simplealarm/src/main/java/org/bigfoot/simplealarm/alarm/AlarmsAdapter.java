/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.alarm;

import java.util.List;

import android.content.Context;

import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.TextView;

import org.bigfoot.simplealarm.R;
import org.bigfoot.simplealarm.SimpleAlarm;
import org.bigfoot.simplealarm.common.SelectableAdapter;
import org.bigfoot.simplealarm.common.IndexOnClickListener;
import org.bigfoot.simplealarm.views.AlarmStateButton;
import org.bigfoot.simplealarm.dialog.DayPickerDialog;

/** @brief Array adapter dedicated to alarm
  */
public class AlarmsAdapter extends SelectableAdapter<AlarmData> {
  private final SimpleAlarm     mContext;   ///< current context
  
  private AlarmStateClickListener  mOnStateClick= null;  ///< Action to perform on state click
  private AlarmTimeClickListener   mOnTimeClick = null;  ///< Action to perform on time set
  
  /** @brief Callback interface for the user clicks on alarm state button
    */
  interface AlarmStateClickListener {
    /** @brief Called on statebutton click.
      * @param[in] v: clicked view
      * @param[in] index: index of the element in the adapter
      */
    void onStateClick(AlarmStateButton v, int index);
  }
  
  /** @brief Callback interface for the user clicks callback
    */
  interface AlarmTimeClickListener {
    /** @brief Called on time view click.
      * @param[in] v: clicked view
      * @param[in] index: index of the element in the adapter
      */
    void onTimeClick(TextView v, int index);
  }
  
  /** @brief Base Constructor
    * @param[in] context: current context
    * @param[in] alarms: alarms list
    */
  public AlarmsAdapter(Context context, List<AlarmData> alarms) {
    super(context, R.layout.alarmrowlayout, alarms);
    this.mContext = (SimpleAlarm) context;
  }
  
  /** @brief Sets the listener for click on the time element.
    * @param[in] onTimeClick: new listener
    */
  public void setOnTimeSetListener(AlarmTimeClickListener onTimeClick) {
    mOnTimeClick = onTimeClick;
  }
  
  /** @brief Sets the state button click listener.
    * @param[in] onStateClick: new listener
    */
  public void setOnStateClickListener(AlarmStateClickListener onStateClick)
  {
    mOnStateClick = onStateClick;
  }
  
  /** @brief Returns the alternative view for display mode.
    *
    * @param[in] index: index of the element in the adapter
    * @param[in] convertView: optional old view to recycle
    * @param[in] parent: parent container
    * @return view with the alarm data
    */
  protected View getDisplayView(int index, View convertView, ViewGroup parent)
  {
    AlarmStateButton displayView = (AlarmStateButton) convertView;
    if(convertView == null)
    {
      displayView = new AlarmStateButton(mContext);
    }
    
    AlarmData alarm = mElements.get(index);
    displayView.setState(alarm.getState());
    displayView.setOnClickListener(new IndexOnClickListener(index) {
      @Override
      public void onClick(View v) {
        onStateClick(v, mIndex);
      }
    });
    return displayView;
  }
  
  @Override
  public void buildView(int index, View rowView, int mode)
  {
    AlarmData alarm = mElements.get(index);
    
    TextView description  = (TextView) rowView.findViewById(R.id.description);
    TextView repeatDays   = (TextView) rowView.findViewById(R.id.repeatDays);
    Button   time         = (Button)   rowView.findViewById(R.id.time);
    
    description.setText(alarm.getName());
    repeatDays.setText(DayPickerDialog.getShortDays(alarm.getDays(), " "));
    time.setText(alarm.getTimeText(mContext));
    time.setOnClickListener(new IndexOnClickListener(index) {
        @Override
        public void onClick(View v) {
          onTimeClick(v, mIndex);
        }
      });

    switch(mode)
    {
      case SELECT_MODE:
        time.setClickable(false);
        break;
      case DISPLAY_MODE:
      default:
        break;
    }
  }
  
  /** @brief Called when user clicks the alarm state button
    *
    * @param[in] v: clicked view
    * @param[in] index: index of the clicked element
    */
  private void onStateClick(View v, int index)
  {
    if(mOnStateClick != null)
    {
      mOnStateClick.onStateClick((AlarmStateButton)v, index);
    }
  }
  
  /** @brief Called when user clicks on the time widget
    *
    * @param[in] v: clicked view
    * @param[in] index: index of the clicked element
    */
  private void onTimeClick(View v, int index)
  {
    if(mOnTimeClick != null && getMode() == DISPLAY_MODE)
    {
      mOnTimeClick.onTimeClick((TextView)v, index);
    }
  }
}

