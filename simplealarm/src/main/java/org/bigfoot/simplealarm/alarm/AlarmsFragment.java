/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.alarm;

import java.util.ArrayList;
import java.util.Calendar;

import android.support.v4.app.FragmentActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.os.Bundle;

import android.text.format.DateFormat;

import android.view.View;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.bigfoot.simplealarm.R;
import org.bigfoot.simplealarm.common.SelectableAdapter;
import org.bigfoot.simplealarm.common.SelectableAdapterFragment;
import org.bigfoot.simplealarm.dialog.TimePickerDialog;
import org.bigfoot.simplealarm.dialog.TimePickerDialogFragment;
import org.bigfoot.simplealarm.notifiable.NotifiableDetailsFragment;
import org.bigfoot.simplealarm.views.AlarmStateButton;


/** @brief Fragment to display the alarms.
  */
public class AlarmsFragment extends SelectableAdapterFragment<AlarmData>
{
  private static final int ALARM_DETAILS_RESULT = 0;  ///< Alarm details result.
  
  private ListView mLVAlarms = null;
  private AlarmSQL mAlarmSQL = null;      ///< Database storing the alarms.

  /** @brief Builder
  * @return Returns the newly created AlarmsFragment.
  */
  public static AlarmsFragment build()
  {
    return new AlarmsFragment();
  }
  
  @Override
  protected AlarmData[] onPopulate()
  {
    return mAlarmSQL.getAlarms();
  }
  
  @Override
  protected void onModeChange(int mode) {}
  
  @Override
  protected View buildFragment(
    LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
  {
    mAlarmSQL = new AlarmSQL(getActivity());
    
    //Log.d("SimpleAlarm", String.valueOf(inflater));
    View view = inflater.inflate(R.layout.alarmsfragment, container, false);

    mLVAlarms = (ListView) view.findViewById(R.id.lvAlarms);
    mLVAlarms.setItemsCanFocus(true);
    mLVAlarms.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id)
      {
        long alarmId = ((AlarmData) parent.getAdapter().getItem(position)).getId();
        
        Intent i = new Intent(getActivity(), AlarmDetails.class);
        i.putExtra(NotifiableDetailsFragment.NOTIFIABLE_ID, alarmId);
        startActivityForResult(i, ALARM_DETAILS_RESULT);
      }
    });
    return view;
  }
  
  @Override
  protected SelectableAdapter buildFragmentAdapter(Context context)
  {
    AlarmsAdapter adapter = new AlarmsAdapter(getActivity(), new ArrayList<AlarmData>());
    adapter.setOnStateClickListener(new AlarmsAdapter.AlarmStateClickListener() {
      @Override
      public void onStateClick(AlarmStateButton v, int index)
      {
        alarmStateChanged(v, index);
      }
    });
    adapter.setOnTimeSetListener(new AlarmsAdapter.AlarmTimeClickListener() {
      @Override
      public void onTimeClick(TextView v, int index)
      {
        showTimeSelection(index);
      }
    });
    
    mLVAlarms.setAdapter(adapter);
    
    return adapter;
  }
  
  /** @brief Called on fragment creation
    * @param[in] savedInstanceState: saved instance state
    */
  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
  }
  
  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    switch(requestCode)
    {
      // AlarmDetails activity returned
      case ALARM_DETAILS_RESULT:
        switch(resultCode)
        {
          case FragmentActivity.RESULT_OK:
            mAdapter.notifyDataSetChanged();
            break;
          case FragmentActivity.RESULT_CANCELED:
          default:
            break;
        }
      default:
        break;
    }
  }
  
  @Override
  protected void onActionElementAdd()
  {
    showTimeSelection(-1);
  }

  /** @brief Shows time selection dialog for the given alarm
   *  @param[in] alarmIndex: index of the alarm to modify in mAdapter
   *    used to initialized the dialog values.
   */
  private void showTimeSelection(int alarmIndex)
  {
    final Calendar c = Calendar.getInstance();
    int hour = c.get(Calendar.HOUR_OF_DAY);
    int min  = c.get(Calendar.MINUTE);
    
    AlarmData alarm = null;
    String pickerTitle = getResources().getString(R.string.newAlarm);
    
    if(alarmIndex >= 0)
    {
      alarm = mAdapter.getItem(alarmIndex);
      pickerTitle = alarm.getName();
      hour  = alarm.getHour();
      min   = alarm.getMinute();
    }
    
    TimePickerOnTimeSetListener onValidate = new TimePickerOnTimeSetListener(alarm);
    TimePickerDialogFragment tpdf = TimePickerDialogFragment.build(
      hour, min, DateFormat.is24HourFormat(getActivity()), pickerTitle, onValidate);
    tpdf.show(getFragmentManager(), "timePickerDialog");
  }
  
  /** @brief Arms the given alarm.
   *  Creates or Modifies an alarm in the alarm database and arms it if required.
   *  @param[in] hour: hour at which the alarm must ring
   *  @param[in] minute: minute at which the alarm must ring
   *  @param[in] alarm: alarm to set, if null, a new alarm will be created
   */
  private void setAlarm(int hour, int minute, AlarmData alarm)
  {
    // if alarm is null creates a new one
    if(alarm == null)
    {
      alarm = new AlarmData();
      alarm.setName(getResources().getString(R.string.newAlarm));
      // add the new alarm in the gui
      mAdapter.add(alarm);
    }
    
    // update the alarm with the hour and minute
    alarm.setTime(hour, minute);
    // activate the new alarm
    alarm.activate();
    
    // updates both database and Gui
    updateAlarm(alarm);
  }
  
  /** @brief Deletes alarm.
    * Cancels the given alarm and removes it from database.
    * @param[in] alarm: index of the alarm to remove
    */
  protected void onDeleteElement(AlarmData alarm)
  {
    // deletes the alarm from database
    // ands remove notification intent
    mAlarmSQL.delAlarm(alarm);
  }
  
  /** @brief Updates the Gui and database with the given alarm.
   *  @param[in] alarm: alarm to set, if null, nothing is done
   */
  private void updateAlarm(AlarmData alarm)
  {
    // Checks that the alarm is not null
    if(alarm != null)
    {
      // updates the alarm in the database
      mAlarmSQL.setAlarm(alarm);

      // update the gui with the new parameters
      mAdapter.notifyDataSetChanged();
    }
  }
  
  /** @brief Callback for user click on the alarm state button in the alarm listview
    *
    * @param[in] v: clicked view
    * @param[in] index: index of the clicked alarm
    */
  private void alarmStateChanged(AlarmStateButton v, int index)
  {
    AlarmData alarm = mAdapter.getItem(index);
    alarm.setState(alarm.nextState());
    updateAlarm(alarm);
    v.setState(alarm.getState());
  }
  
  /** @brief Click Listener for the TimePickerDialogFragment
    *
    * Should be bound to the positive button,
    * it will tells the OnTimeSetListener to apply the changes
    * when it will be triggered.
    */
  private class TimePickerOnTimeSetListener
    implements DialogInterface.OnClickListener
  {
    private AlarmData mAlarm = null;  ///< alarm to update
    
    /** Base constructor
      * @param[in] alarm: alarm to update once triggered
      */
    TimePickerOnTimeSetListener(AlarmData alarm)
    {
      super();
      this.mAlarm = alarm;
    }
    
    /** @brief Applies the selected time to the AlarmData attribute.
      * @param[in] d: dialog interface
      * @param[in] which: dialog button
      */
    @Override
    public void onClick(DialogInterface d, int which)
    {
      TimePickerDialog tpd = (TimePickerDialog) d;
      int hour = tpd.getHour();
      int minute = tpd.getMinute();
      setAlarm(hour, minute, mAlarm);
    }
  }
}
