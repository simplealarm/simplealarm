/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.alarm;

import android.content.Intent;
import android.widget.TextView;
import android.widget.Toast;

import org.bigfoot.simplealarm.ChoiceBar;
import org.bigfoot.simplealarm.R;
import org.bigfoot.simplealarm.notifiable.NotifiableData;
import org.bigfoot.simplealarm.notifiable.NotifiableNotifier;

/** @brief Activity started for alarm notification*/
public class AlarmNotifier extends NotifiableNotifier {
  private AlarmSQL  mAlarmSQL = null; ///< database containing alarms
  private AlarmData mAlarm    = null; ///< alarm corresponding to the intent

  protected void onNotificationStart()
  {
    mAlarmSQL.delayAlarm(mAlarm);
  }

  /** @brief If a new alarm occurs while still ringing, the new alarm is canceled.*/
  @Override
  protected void handleNewNotifiable(NotifiableData newNotifiable)
  {
    mAlarmSQL.alarmNotified((AlarmData) newNotifiable);
    String message = String.format(getResources().getString(R.string.alarmElapsedMessage),
                                   newNotifiable.getName());
    Toast.makeText( getApplicationContext(), message, Toast.LENGTH_LONG ).show();
  }

  @Override
  protected NotifiableData buildNotifiable(Intent i)
  {
    final long alarmId = getNotifiableId(i);
    AlarmData ans = null;
    if(alarmId >= 0)
    {
      ans = mAlarmSQL.getAlarmFromId(alarmId);
    }
    return ans;
  }

  @Override
  protected NotifiableData initNotifiable(Intent i)
  {
    mAlarmSQL = new AlarmSQL(this);
    mAlarm = (AlarmData) buildNotifiable(i);
    return mAlarm;
  }
  
  @Override
  protected int getContentId()
  {
    return R.layout.alarmnotifier;
  }
  
  @Override
  protected void updateGUI() {
    TextView alarmNameTV = (TextView) findViewById(R.id.notifierTitle);
    ChoiceBar wakeLevel = (ChoiceBar) findViewById(R.id.wakeBar);

    alarmNameTV.setText(mAlarm.getName());

    wakeLevel.setOnChoiceListener(new ChoiceBar.OnChoiceListener() {
      @Override
      public void onChoice(int choice) {
        if(choice == 0)
          AlarmNotifier.this.alarmDelay();
        else
          AlarmNotifier.this.alarmStop();
      }
    });
  }
  
  /** Stops the alarm */
  private void alarmStop() {
    mAlarmSQL.alarmNotified(mAlarm);
    close();
  }
  
  /** Delays the alarm, alarm will ring again in a few minutes*/
  private void alarmDelay() {
    close();
  }
}
