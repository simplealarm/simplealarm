/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.alarm;

import java.util.Calendar;

import android.content.Context;

import org.bigfoot.simplealarm.notifiable.NotifiableData;

/** @brief Describes an alarm.
  *
  * Contains the parameters of an alarm:
  * - name,
  * - state,
  * - ring time,
  * - repeat configuration.
  */
public class AlarmData extends NotifiableData {
  public final static int INACTIVE = 0; ///< Inactive state
  public final static int ACTIVE   = 1; ///< Active state
  public final static int SKIPPED  = 2; ///< Won't ring at the next schedule
  public final static int RINGING  = 3; ///< Alarm ringing or delayed
  
    /**< This is the primary key in the alarm database. If the alarm is not registered, value is -1.*/
  private int     mHour   = 0;       ///< Ring hour of the alarm
  private int     mMinute = 0;       ///< Ring minute of the alarm
  private int     mRepeat = 0;       ///< One bit for each week-day (SMTWTFS)
  
  /** @brief Constructor
    * @param[in] name: name for the new alarm
    * @param[in] state: state of the new alarm
    * @param[in] hour: ring hour of the alarm
    * @param[in] minute: ring minute of the alram
    * @param[in] repeat: integer representing the repeat days
    */
  public AlarmData(String name, int state, int hour, int minute, int repeat)
  {
    this.mName   = name;
    this.mState  = state;
    this.mHour   = hour;
    this.mMinute = minute;
    this.mRepeat = repeat;
  }
  
  /** @brief Base Constructor */
  public AlarmData() {}
  
  /** @brief Sets the alarm state to ACTIVE. */
  public void activate()
  {
    mState = ACTIVE;
  }
  
  /** Called to deactivate the given alarm
   */
  public void deactivate()
  {
    mState = INACTIVE;
  }

  /** @brief Sets the state of the alarm
    * @param[in] state: The state of the alarm.
    * 
    * Sets the alarm in one of the states:
    * - ACTIVE
    * - INACTIVE
    * - SKIPPED
    * - RINGING
    */
  @Override
  public void setState(int state)
  {
    mState = state;
    switch(state)
    {
      case(AlarmData.ACTIVE):
      case(AlarmData.INACTIVE):
      case(AlarmData.SKIPPED):
      case(AlarmData.RINGING):
        break;
      default:
        mState = AlarmData.INACTIVE;
    }
  }
  
  /** @brief Gets the next state of the alarm.
    *
    * Calculates the next state of the alarm. The current state of the alarm remains unchanged.
    * @return next state of the alarm.
    */
  @Override
  public int nextState()
  {
    int newState;
    switch(mState)
    {
      case INACTIVE:
      case RINGING:
        newState = ACTIVE;
        break;
      case ACTIVE:
        newState = SKIPPED;
        if(getRepeat() == 0)
          newState = INACTIVE;
        break;
      case AlarmData.SKIPPED:
        newState = INACTIVE;
        break;
      default:
        newState = INACTIVE;
        break;
    }
    return newState;
  }
  
  /** @brief Sets the alarm's ring time.
    * @param[in] hour: hour of the ring time
    * @param[in] minute: minute of the ring time
    */
  public void setTime(int hour, int minute)
  {
    mHour   = hour;
    mMinute = minute;
  }

  /** @brief Sets the repeat days from a list.
    * @param[in] days: list of booleans with true for each active day.
    *
    * Days starts at index 1 with Sunday.
    */
  public void setDays(boolean[] days)
  {
    int repeat = 0;
    for(int i = 1; i < days.length; ++i)
    {
      if(days[i])
      {
        repeat += 1 << (i-1);
      }
    }
    mRepeat = repeat;
  }
  
  /** @brief Gets the repeat days from a list.
    * @return List of booleans with true for each active day.
    *
    * Days starts at index 1 with Sunday.
    */
  public boolean[] getDays()
  {
    boolean[] days = new boolean[8];
    for(int i = 0; i < 7; ++i)
    {
      days[i + 1] = (((mRepeat >> i) & 1) == 1);
    }
    return days;
  }

  /** @brief Gets the alarm hour.
    * @return Alarm's hour.
    */
  public int getHour()
  {
    return mHour;
  }
  
  /** @brief Gets the alarm minute.
    * @return Alarm's minute.
    */
  public int getMinute()
  {
    return mMinute;
  }
  
  /** @brief Gets the alarm repeat days.
    * @return Alarm's repeat days.
    */
  public int getRepeat()
  {
    return mRepeat;
  }
  
  /** @brief Gets the ring time as text
    * @param[in] context: context used to get the locals parameters.
    * @return Ringtime as text.
    */
  public String getTimeText(Context context)
  {
    return getTimeText(context, mHour, mMinute);
  }
  
  /** @brief Gets the text format for hour/min combination
    * @param[in] context: context used to get the locals parameters.
    * @param[in] hour: hour
    * @param[in] minute: minute
    * @return Ringtime as text.
    */
  private static String getTimeText(Context context, int hour, int minute)
  {
    Calendar c = Calendar.getInstance();
    c.set(Calendar.HOUR_OF_DAY, hour);
    c.set(Calendar.MINUTE, minute);
    
    return android.text.format.DateFormat.getDateFormat(
      context.getApplicationContext()).getTimeInstance(
      java.text.DateFormat.SHORT).format(c.getTime());
  }
}

