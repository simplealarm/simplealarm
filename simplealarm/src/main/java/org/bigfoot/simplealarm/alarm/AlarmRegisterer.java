/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarm.alarm;

import android.app.PendingIntent;

import android.content.Context;
import android.content.res.Resources;
import android.widget.Toast;

import java.util.Calendar;

import org.bigfoot.simplealarm.R;
import org.bigfoot.simplealarm.notifiable.NotifiableRegisterer;

public class AlarmRegisterer extends NotifiableRegisterer {
  //public static final String ALARM_ID = "Id"; ///< Name for the Alarm Id parameter
  
  private static final int    DELAY = 5; ///< delay in minute for reporting the alarm
  
  public AlarmRegisterer(Context context)
  {
    super(context);
  }
  
  /** @brief Arms the given alarm.
    *
    * Checks the state of the alarm and arms it if required.
    * @param[in] alarm: alarm to arm
    * @return Time at which the alarm will ring.
    */
  public long setAlarm(AlarmData alarm)
  {
    long id = alarm.getId();
    long ringTime = 0;
    
    // If the alarm Id is null, the alarm is not in the database, nothing is done
    if(id != -1)
    {
      // creates the pending intent for alarm notification
      PendingIntent pendingIntent = makePendingIntent(alarm);
      
      int state = alarm.getState();
      
      switch(state)
      {
        case AlarmData.INACTIVE:
          alarmManagerCancel(pendingIntent);
          break;
        case AlarmData.ACTIVE:
        case AlarmData.SKIPPED:
        case AlarmData.RINGING:
          // register the pending intent
          ringTime = alarmManagerSet(pendingIntent, alarm);
          break;
        default:
          break;
      }
    }
    return ringTime;
  }

  /**Sets the alarm manager for the given alarm.
   *
   * @param i: pending Intent to start
   * @param alarm: alarm to set
   * @return Next ring time
   */
  private long alarmManagerSet(PendingIntent i, AlarmData alarm)
  {
    long[] ringTime = new long[1];
    alarmManagerSet(getAlarmTime(alarm, ringTime), i);
    toastRingTime(ringTime[0], alarm);
    return ringTime[0];
  }

  /** @brief Delays the alarm in DELAY minutes
    * 
    * Called if the user ignored the alarm. The alarm is then delayed for a few minutes.
    * @param[in] alarm: alarm to delay
    */
  public void delayAlarm(AlarmData alarm)
  {
    // creates the pending intent for alarm notification
    PendingIntent pendingIntent = makePendingIntent(alarm);
    // calculate the delay date
    Calendar alarmTime = Calendar.getInstance();
    alarmTime.add(Calendar.MINUTE, DELAY);
    alarmManagerSet(pendingIntent, alarm);
  }
  
  @Override
  protected Class getIntentClass(){return AlarmReceiver.class;}
  
  /** @brief Gets the next alarm date in milliseconds.
    *
    * Calculates the next schedule time for the input alarm. Alarms can be skipped, in this case the
    * schedule time can be different from the ringtime. Both are returned separately.
    * 
    * @param[in] alarm: alarm for the scheduling
    * @param[out] ringTime: 1 element long array used to return the next ringtime.
    * @return Schedule time in millis.
    */
  private long getAlarmTime(AlarmData alarm, long[] ringTime)
  {
    int hour   = alarm.getHour();
    int minute = alarm.getMinute();

    Calendar alarmTime = Calendar.getInstance();
    if (alarm.getState() == AlarmData.RINGING) {
      alarmTime.add(Calendar.MINUTE, DELAY);
      ringTime[0] = alarmTime.getTimeInMillis();
    }
    else {
      // set alarm hour and minute
      alarmTime.set(Calendar.HOUR_OF_DAY, hour);
      alarmTime.set(Calendar.MINUTE, minute);
      alarmTime.set(Calendar.SECOND, 0);
      // calculates alarm day
      ringTime[0] = setAlarmDay(alarm, alarmTime);
    }
    return alarmTime.getTimeInMillis();
  }
  
  /** Calculate the day for the next alarm and set it in the input calendar
   *  @param[in] alarm: Alarm object used to find the next alarm Day
   *  @param[in,out] alarmTime: Next Schedule time for the alarm
   *  @return Next ring time of the alarm (different from schedule time if skip is active)
   */
  private long setAlarmDay(AlarmData alarm, Calendar alarmTime)
  {
    Calendar now = Calendar.getInstance();
    boolean[] days = alarm.getDays();
    boolean repeat = false;
    boolean skipped = (alarm.getState() == AlarmData.SKIPPED);
    // Ring time and schedule time of the pending intent might differ if skip is active
    long scheduleTime = 0; // The alarm will be checked at that time
    long ringTime = 0;     // The alarm will ring at that time.
    // check if repeat is set
    for(int i = 1; i < 8; ++i)
    {
      if(days[i])
      {
        repeat = true;
        break;
      }
    }
    
    // repeat no set ...
    if(!repeat)
    {
      // ... add one day if the alarm time has passed
      if(alarmTime.before(now))
      {
        alarmTime.add(Calendar.DAY_OF_MONTH, 1);
      }
      scheduleTime = alarmTime.getTimeInMillis();
      ringTime = scheduleTime;
    }
    else
    {
      int today = now.get(Calendar.DAY_OF_WEEK);
      boolean findNextDay = true;
      //Check whether the alarm must ring today...
      if(days[today])
      {
        // check whether the time has passed...
        if(now.before(alarmTime))
        {
          scheduleTime = alarmTime.getTimeInMillis();
          if(skipped)
          {
            // We still need to find the ringtime
            findNextDay = true;
          }
          else
          {
            // RingTime is the same as ScheduleTime
            findNextDay = false;
            ringTime = scheduleTime;
          }
        }
      }
      // ...set the alarm to the next day of week found in days[]
      if(findNextDay)
      {
        while((scheduleTime == 0) || (ringTime == 0))
        {
          alarmTime.add(Calendar.DAY_OF_MONTH, 1);
          // Check wether the alarm is active for the current alarmTime day
          if(days[alarmTime.get(Calendar.DAY_OF_WEEK)])
          {
            if(scheduleTime == 0)
            {
              scheduleTime = alarmTime.getTimeInMillis();
              // if alarm is not skipped, ringTime and scheduleTime are the same
              if(!skipped)
                ringTime = scheduleTime;
            }
            else
            {
              // We are here because alarm is skipped
              ringTime = alarmTime.getTimeInMillis();
            }
          }
        }
      }
    }
    alarmTime.setTimeInMillis(scheduleTime);
    return ringTime;
  }

    /**@brief Toasts next alarm ringtime.
     * @param[in] ringTime: ring time delay in milliseconds
     */
    private void toastRingTime(long ringTime, AlarmData alarm)
    {
        Resources res = mContext.getResources();
        long now = Calendar.getInstance().getTimeInMillis();

        long diff = ringTime - now;
        if(diff > 0)
        {
            long days = diff / (24*3600*1000);
            diff = diff % (24*3600*1000);
            long hours = diff / (3600*1000);
            diff = diff % (3600 * 1000);
            long mins = diff / (60 * 1000);

            String message;
            String baseMessage = res.getString(R.string.ringInMessage);

            // string for units, select singular or plural
            String daysString;
            String hoursString;
            String minsString;
            if(days > 1)
            {
                daysString = res.getString(R.string.days);
            }
            else
            {
                daysString = res.getString(R.string.day);
            }
            if(hours > 1)
            {
                hoursString = res.getString(R.string.hours);
            }
            else
            {
                hoursString = res.getString(R.string.hour);
            }
            if(mins > 1)
            {
                minsString = res.getString(R.string.mins);
            }
            else
            {
                minsString = res.getString(R.string.min);
            }

            // computes and display the toast for next alarm time
            String timePart = String.format("%d %s, %d %s, %d %s",
                    days, daysString, hours, hoursString, mins, minsString);
            message = String.format(baseMessage, alarm.getName(), timePart);

            Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
        }
    }
}
